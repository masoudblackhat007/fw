<?php



if (isset($_SERVER['REQUEST_METHOD'])) {
    $router = new Bramus\Router\Router();
    $router->setNamespace("builder");
    $router->setNamespace("App\Controllers");
    $router->set404("/.*", "HomeController@notFound");
//$router->get("/","HomeController@index");
    $router->get('/register', 'UserController@register');
    $router->post('/register', 'UserController@register');
    $router->get('/login', 'UserController@login');
    $router->post('/login', 'UserController@login');
    $router->get('/csrf', 'UserController@csrf');
    $router->get('/block', 'UserController@block');
    $router->get('/logout', 'UserController@logout');
    $router->get('/admin', 'PanelController@panel');
    $router->get('/test', 'TestDesignController@test');
    $router->post('/admin/category', 'CategoryController@create');
    $router->get('/admin/category', 'CategoryController@create');
    $router->before('GET|POST', '/admin', 'GlobalController@loadMiddleware');
    $router->before('GET|POST', '/admin/.*', 'GlobalController@loadMiddleware');
    $router->run();
}
