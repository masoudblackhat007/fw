<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit90d752933af3bfa76cf3069a75a11f60
{
    public static $files = array (
        '6e3fae29631ef280660b3cdad06f25a8' => __DIR__ . '/..' . '/symfony/deprecation-contracts/function.php',
        'a4a119a56e50fbb293281d9a48007e0e' => __DIR__ . '/..' . '/symfony/polyfill-php80/bootstrap.php',
        '320cde22f66dd4f5d3fd621d3e88b98f' => __DIR__ . '/..' . '/symfony/polyfill-ctype/bootstrap.php',
        '0e6d7bf4a5811bfa5cf40c5ccd6fae6a' => __DIR__ . '/..' . '/symfony/polyfill-mbstring/bootstrap.php',
        '23c18046f52bef3eea034657bafda50f' => __DIR__ . '/..' . '/symfony/polyfill-php81/bootstrap.php',
        '8825ede83f2f289127722d4e842cf7e8' => __DIR__ . '/..' . '/symfony/polyfill-intl-grapheme/bootstrap.php',
        'e69f7f6ee287b969198c3c9d6777bd38' => __DIR__ . '/..' . '/symfony/polyfill-intl-normalizer/bootstrap.php',
        '0d59ee240a4cd96ddbb4ff164fccea4d' => __DIR__ . '/..' . '/symfony/polyfill-php73/bootstrap.php',
        'b6b991a57620e2fb6b2f66f03fe9ddc2' => __DIR__ . '/..' . '/symfony/string/Resources/functions.php',
        '60799491728b879e74601d83e38b2cad' => __DIR__ . '/..' . '/illuminate/collections/helpers.php',
        'a1105708a18b76903365ca1c4aa61b02' => __DIR__ . '/..' . '/symfony/translation/Resources/functions.php',
        '72579e7bd17821bb1321b87411366eae' => __DIR__ . '/..' . '/illuminate/support/helpers.php',
        '72142d7b40a3a0b14e91825290b5ad82' => __DIR__ . '/..' . '/cakephp/core/functions.php',
        'ef65a1626449d89d0811cf9befce46f0' => __DIR__ . '/..' . '/illuminate/events/functions.php',
        '948ad5488880985ff1c06721a4e447fe' => __DIR__ . '/..' . '/cakephp/utility/bootstrap.php',
        'cf97c57bfe0f23854afd2f3818abb7a0' => __DIR__ . '/..' . '/zendframework/zend-diactoros/src/functions/create_uploaded_file.php',
        '9bf37a3d0dad93e29cb4e1b1bfab04e9' => __DIR__ . '/..' . '/zendframework/zend-diactoros/src/functions/marshal_headers_from_sapi.php',
        'ce70dccb4bcc2efc6e94d2ee526e6972' => __DIR__ . '/..' . '/zendframework/zend-diactoros/src/functions/marshal_method_from_sapi.php',
        'f86420df471f14d568bfcb71e271b523' => __DIR__ . '/..' . '/zendframework/zend-diactoros/src/functions/marshal_protocol_version_from_sapi.php',
        'b87481e008a3700344428ae089e7f9e5' => __DIR__ . '/..' . '/zendframework/zend-diactoros/src/functions/marshal_uri_from_sapi.php',
        '0b0974a5566a1077e4f2e111341112c1' => __DIR__ . '/..' . '/zendframework/zend-diactoros/src/functions/normalize_server.php',
        '1ca3bc274755662169f9629d5412a1da' => __DIR__ . '/..' . '/zendframework/zend-diactoros/src/functions/normalize_uploaded_files.php',
        '40360c0b9b437e69bcbb7f1349ce029e' => __DIR__ . '/..' . '/zendframework/zend-diactoros/src/functions/parse_cookie_header.php',
        '061862578d24ccd94b2e0776e57bf341' => __DIR__ . '/../..' . '/configs/configs.php',
        '7d0c7101abbbe13d46098fcddf9baee0' => __DIR__ . '/../..' . '/systems/Function.php',
        '93d5efe3d95a5c91ffd5b3fb6e2186d3' => __DIR__ . '/../..' . '/systems/Helpers/Validation.php',
        'e6e964f2672521ad229121c7363a22d9' => __DIR__ . '/../..' . '/systems/Helpers/View.php',
        'ed495b7e850812141666a1157b6fbf04' => __DIR__ . '/../..' . '/systems/Helpers/Security.php',
        'dfa0c01c54c8ffc548c5626e637e28a2' => __DIR__ . '/../..' . '/routers.php',
    );

    public static $prefixLengthsPsr4 = array (
        'v' => 
        array (
            'voku\\' => 5,
        ),
        'Z' => 
        array (
            'Zend\\Diactoros\\' => 15,
        ),
        'V' => 
        array (
            'Validation\\User\\' => 16,
        ),
        'S' => 
        array (
            'Symfony\\Polyfill\\Php81\\' => 23,
            'Symfony\\Polyfill\\Php80\\' => 23,
            'Symfony\\Polyfill\\Php73\\' => 23,
            'Symfony\\Polyfill\\Mbstring\\' => 26,
            'Symfony\\Polyfill\\Intl\\Normalizer\\' => 33,
            'Symfony\\Polyfill\\Intl\\Grapheme\\' => 31,
            'Symfony\\Polyfill\\Ctype\\' => 23,
            'Symfony\\Contracts\\Translation\\' => 30,
            'Symfony\\Contracts\\Service\\' => 26,
            'Symfony\\Component\\Yaml\\' => 23,
            'Symfony\\Component\\Translation\\' => 30,
            'Symfony\\Component\\String\\' => 25,
            'Symfony\\Component\\Finder\\' => 25,
            'Symfony\\Component\\Filesystem\\' => 29,
            'Symfony\\Component\\Console\\' => 26,
            'Symfony\\Component\\Config\\' => 25,
        ),
        'R' => 
        array (
            'Requtize\\QueryBuilder\\' => 22,
        ),
        'P' => 
        array (
            'Psr\\SimpleCache\\' => 16,
            'Psr\\Log\\' => 8,
            'Psr\\Http\\Message\\' => 17,
            'Psr\\Container\\' => 14,
            'Psr\\Cache\\' => 10,
            'PhpRestfulApiResponse\\' => 22,
            'Phinx\\' => 6,
        ),
        'O' => 
        array (
            'OpenApi\\' => 8,
        ),
        'L' => 
        array (
            'League\\Fractal\\' => 15,
        ),
        'J' => 
        array (
            'Jenssegers\\Blade\\' => 17,
        ),
        'I' => 
        array (
            'Illuminate\\View\\' => 16,
            'Illuminate\\Support\\' => 19,
            'Illuminate\\Pipeline\\' => 20,
            'Illuminate\\Filesystem\\' => 22,
            'Illuminate\\Events\\' => 18,
            'Illuminate\\Contracts\\' => 21,
            'Illuminate\\Container\\' => 21,
            'Illuminate\\Bus\\' => 15,
        ),
        'D' => 
        array (
            'Doctrine\\Inflector\\' => 19,
            'Doctrine\\Common\\Lexer\\' => 22,
            'Doctrine\\Common\\Annotations\\' => 28,
        ),
        'C' => 
        array (
            'Carbon\\' => 7,
            'Cake\\Utility\\' => 13,
            'Cake\\Datasource\\' => 16,
            'Cake\\Database\\' => 14,
            'Cake\\Core\\' => 10,
        ),
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'voku\\' => 
        array (
            0 => __DIR__ . '/..' . '/voku/portable-ascii/src/voku',
        ),
        'Zend\\Diactoros\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-diactoros/src',
        ),
        'Validation\\User\\' => 
        array (
            0 => __DIR__ . '/..' . '/validation/user/valid',
        ),
        'Symfony\\Polyfill\\Php81\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-php81',
        ),
        'Symfony\\Polyfill\\Php80\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-php80',
        ),
        'Symfony\\Polyfill\\Php73\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-php73',
        ),
        'Symfony\\Polyfill\\Mbstring\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-mbstring',
        ),
        'Symfony\\Polyfill\\Intl\\Normalizer\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-intl-normalizer',
        ),
        'Symfony\\Polyfill\\Intl\\Grapheme\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-intl-grapheme',
        ),
        'Symfony\\Polyfill\\Ctype\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-ctype',
        ),
        'Symfony\\Contracts\\Translation\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/translation-contracts',
        ),
        'Symfony\\Contracts\\Service\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/service-contracts',
        ),
        'Symfony\\Component\\Yaml\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/yaml',
        ),
        'Symfony\\Component\\Translation\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/translation',
        ),
        'Symfony\\Component\\String\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/string',
        ),
        'Symfony\\Component\\Finder\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/finder',
        ),
        'Symfony\\Component\\Filesystem\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/filesystem',
        ),
        'Symfony\\Component\\Console\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/console',
        ),
        'Symfony\\Component\\Config\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/config',
        ),
        'Requtize\\QueryBuilder\\' => 
        array (
            0 => __DIR__ . '/..' . '/requtize/query-builder/src/QueryBuilder',
        ),
        'Psr\\SimpleCache\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/simple-cache/src',
        ),
        'Psr\\Log\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/log/Psr/Log',
        ),
        'Psr\\Http\\Message\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/http-message/src',
        ),
        'Psr\\Container\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/container/src',
        ),
        'Psr\\Cache\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/cache/src',
        ),
        'PhpRestfulApiResponse\\' => 
        array (
            0 => __DIR__ . '/..' . '/harryosmar/php-restful-api-response/src',
        ),
        'Phinx\\' => 
        array (
            0 => __DIR__ . '/..' . '/robmorgan/phinx/src/Phinx',
        ),
        'OpenApi\\' => 
        array (
            0 => __DIR__ . '/..' . '/zircote/swagger-php/src',
        ),
        'League\\Fractal\\' => 
        array (
            0 => __DIR__ . '/..' . '/league/fractal/src',
        ),
        'Jenssegers\\Blade\\' => 
        array (
            0 => __DIR__ . '/..' . '/jenssegers/blade/src',
        ),
        'Illuminate\\View\\' => 
        array (
            0 => __DIR__ . '/..' . '/illuminate/view',
        ),
        'Illuminate\\Support\\' => 
        array (
            0 => __DIR__ . '/..' . '/illuminate/collections',
            1 => __DIR__ . '/..' . '/illuminate/macroable',
            2 => __DIR__ . '/..' . '/illuminate/support',
        ),
        'Illuminate\\Pipeline\\' => 
        array (
            0 => __DIR__ . '/..' . '/illuminate/pipeline',
        ),
        'Illuminate\\Filesystem\\' => 
        array (
            0 => __DIR__ . '/..' . '/illuminate/filesystem',
        ),
        'Illuminate\\Events\\' => 
        array (
            0 => __DIR__ . '/..' . '/illuminate/events',
        ),
        'Illuminate\\Contracts\\' => 
        array (
            0 => __DIR__ . '/..' . '/illuminate/contracts',
        ),
        'Illuminate\\Container\\' => 
        array (
            0 => __DIR__ . '/..' . '/illuminate/container',
        ),
        'Illuminate\\Bus\\' => 
        array (
            0 => __DIR__ . '/..' . '/illuminate/bus',
        ),
        'Doctrine\\Inflector\\' => 
        array (
            0 => __DIR__ . '/..' . '/doctrine/inflector/lib/Doctrine/Inflector',
        ),
        'Doctrine\\Common\\Lexer\\' => 
        array (
            0 => __DIR__ . '/..' . '/doctrine/lexer/lib/Doctrine/Common/Lexer',
        ),
        'Doctrine\\Common\\Annotations\\' => 
        array (
            0 => __DIR__ . '/..' . '/doctrine/annotations/lib/Doctrine/Common/Annotations',
        ),
        'Carbon\\' => 
        array (
            0 => __DIR__ . '/..' . '/nesbot/carbon/src/Carbon',
        ),
        'Cake\\Utility\\' => 
        array (
            0 => __DIR__ . '/..' . '/cakephp/utility',
        ),
        'Cake\\Datasource\\' => 
        array (
            0 => __DIR__ . '/..' . '/cakephp/datasource',
        ),
        'Cake\\Database\\' => 
        array (
            0 => __DIR__ . '/..' . '/cakephp/database',
        ),
        'Cake\\Core\\' => 
        array (
            0 => __DIR__ . '/..' . '/cakephp/core',
        ),
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/app',
        ),
    );

    public static $prefixesPsr0 = array (
        'B' => 
        array (
            'Bramus' => 
            array (
                0 => __DIR__ . '/..' . '/bramus/router/src',
            ),
        ),
    );

    public static $classMap = array (
        'Admin' => __DIR__ . '/../..' . '/app/Middlewares/Admin.php',
        'App\\Controllers\\CategoryController' => __DIR__ . '/../..' . '/app/Controllers/CategoryController.php',
        'App\\Controllers\\GlobalController' => __DIR__ . '/../..' . '/app/Controllers/GlobalController.php',
        'App\\Controllers\\HomeController' => __DIR__ . '/../..' . '/app/Controllers/HomeController.php',
        'App\\Controllers\\PanelController' => __DIR__ . '/../..' . '/app/Controllers/PanelController.php',
        'App\\Controllers\\UserController' => __DIR__ . '/../..' . '/app/Controllers/UserController.php',
        'App\\DesignPattern\\Book\\Proxy\\ProxyBookList' => __DIR__ . '/../..' . '/app/DesignPattern/Proxy/Book/ProxyBookList.php',
        'App\\DesignPattern\\Database\\Database' => __DIR__ . '/../..' . '/app/DesignPattern/Facade/Database/Database.php',
        'App\\DesignPattern\\Database\\Interfaces\\DatabaseInterfaces' => __DIR__ . '/../..' . '/app/DesignPattern/Facade/Database/Interfaces/DatabaseInterfaces.php',
        'App\\DesignPattern\\Database\\Interfaces\\DatabaseLogInterfaces' => __DIR__ . '/../..' . '/app/DesignPattern/Facade/Database/Interfaces/DatabaseLogInterfaces.php',
        'App\\DesignPattern\\Database\\SubClasses\\DatabaseClass' => __DIR__ . '/../..' . '/app/DesignPattern/Facade/Database/SubClasses/DatabaseClass.php',
        'App\\DesignPattern\\Database\\SubClasses\\LogClass' => __DIR__ . '/../..' . '/app/DesignPattern/Facade/Database/SubClasses/LogClass.php',
        'App\\DesignPattern\\Decorator\\BookingDecorator' => __DIR__ . '/../..' . '/app/DesignPattern/Decorator/BookingDecorator.php',
        'App\\DesignPattern\\Decorator\\Classes\\DoubleRoomBooking' => __DIR__ . '/../..' . '/app/DesignPattern/Decorator/Classes/DoubleRoomBooking.php',
        'App\\DesignPattern\\Decorator\\Classes\\ExtraBed' => __DIR__ . '/../..' . '/app/DesignPattern/Decorator/Classes/ExtraBed.php',
        'App\\DesignPattern\\Decorator\\Classes\\WiFi' => __DIR__ . '/../..' . '/app/DesignPattern/Decorator/Classes/WiFi.php',
        'App\\DesignPattern\\Decorator\\interfaceBooking\\Booking' => __DIR__ . '/../..' . '/app/DesignPattern/Decorator/Interfaces/Booking.php',
        'App\\DesignPattern\\DependencyInjection\\Configs\\DatabaseConfiguration' => __DIR__ . '/../..' . '/app/DesignPattern/DependencyInjection/Configs/DatabaseConfiguration.php',
        'App\\DesignPattern\\DependencyInjection\\DatabaseConnection' => __DIR__ . '/../..' . '/app/DesignPattern/DependencyInjection/DatabaseConnection.php',
        'App\\DesignPattern\\Proxy\\Book\\Classes\\Book' => __DIR__ . '/../..' . '/app/DesignPattern/Proxy/Book/Classes/Book.php',
        'App\\DesignPattern\\Proxy\\Book\\Classes\\BookList' => __DIR__ . '/../..' . '/app/DesignPattern/Proxy/Book/Classes/BookList.php',
        'App\\DesignPattern\\Proxy\\Book\\Interfaces\\BookListInterface' => __DIR__ . '/../..' . '/app/DesignPattern/Proxy/Book/Interfaces/BookListInterface.php',
        'App\\DesignPattern\\Proxy\\Wallet\\WalletProxy' => __DIR__ . '/../..' . '/app/DesignPattern/Proxy/Wallet/WalletProxy.php',
        'App\\Services\\User\\DesignPattern\\Strategy\\Methods\\EmailLogin' => __DIR__ . '/../..' . '/app/Services/User/DesignPattern/Strategy/Methods/EmailLogin.php',
        'App\\Services\\User\\Interfaces\\LoginInterface' => __DIR__ . '/../..' . '/app/Services/User/Interfaces/LoginInterface.php',
        'App\\Services\\User\\Strategist\\EmailLogin' => __DIR__ . '/../..' . '/app/Services/User/Strategist/EmailLogin.php',
        'App\\Services\\User\\Strategist\\OtpLogin' => __DIR__ . '/../..' . '/app/Services/User/Strategist/OtpLogin.php',
        'Attribute' => __DIR__ . '/..' . '/symfony/polyfill-php80/Resources/stubs/Attribute.php',
        'Category' => __DIR__ . '/../..' . '/app/Request/Category.php',
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
        'Controllers' => __DIR__ . '/../..' . '/systems/Helpers/Controllers.php',
        'Database' => __DIR__ . '/../..' . '/systems/Helpers/Database.php',
        'Factory' => __DIR__ . '/..' . '/validation/user/valid/Factory.php',
        'Hash\\Hash' => __DIR__ . '/../..' . '/app/DesignPattern/Facade/Hash/Hash.php',
        'Hash\\Interfaces\\HashInterfaces' => __DIR__ . '/../..' . '/app/DesignPattern/Facade/Hash/Interfaces/HashInterfaces.php',
        'Hash\\Methods\\Md5' => __DIR__ . '/../..' . '/app/DesignPattern/Facade/Hash/Methods/Md5.php',
        'Hash\\Methods\\Sh1' => __DIR__ . '/../..' . '/app/DesignPattern/Facade/Hash/Methods/Sh1.php',
        'JsonException' => __DIR__ . '/..' . '/symfony/polyfill-php73/Resources/stubs/JsonException.php',
        'Login' => __DIR__ . '/../..' . '/app/Middlewares/Login.php',
        'LoginInterface' => __DIR__ . '/../..' . '/app/Services/User/DesignPattern/Strategy/Interfaces/LoginInterface.php',
        'Normalizer' => __DIR__ . '/..' . '/symfony/polyfill-intl-normalizer/Resources/stubs/Normalizer.php',
        'OtpLogin' => __DIR__ . '/../..' . '/app/Services/User/DesignPattern/Strategy/Methods/OtpLogin.php',
        'PhpToken' => __DIR__ . '/..' . '/symfony/polyfill-php80/Resources/stubs/PhpToken.php',
        'QueryBuilderDB' => __DIR__ . '/../..' . '/systems/Helpers/QueryBuilderDB.php',
        'ReturnTypeWillChange' => __DIR__ . '/..' . '/symfony/polyfill-php81/Resources/stubs/ReturnTypeWillChange.php',
        'Stringable' => __DIR__ . '/..' . '/symfony/polyfill-php80/Resources/stubs/Stringable.php',
        'System\\Facade' => __DIR__ . '/../..' . '/app/DesignPattern/Facade/System/Facade.php',
        'System\\Interfaces\\SystemInterface' => __DIR__ . '/../..' . '/app/DesignPattern/Facade/System/Interfaces/SystemInterface.php',
        'System\\Interfaces\\SystemOperating' => __DIR__ . '/../..' . '/app/DesignPattern/Facade/System/Interfaces/SystemOperating.php',
        'System\\Methods\\Subsystem1' => __DIR__ . '/../..' . '/app/DesignPattern/Facade/System/Methods/Subsystem1.php',
        'System\\Methods\\Subsystem2' => __DIR__ . '/../..' . '/app/DesignPattern/Facade/System/Methods/Subsystem2.php',
        'UnhandledMatchError' => __DIR__ . '/..' . '/symfony/polyfill-php80/Resources/stubs/UnhandledMatchError.php',
        'User' => __DIR__ . '/../..' . '/app/Models/User.php',
        'UserPasswordLogin' => __DIR__ . '/../..' . '/app/Services/User/DesignPattern/Strategy/Methods/UserPasswordLogin.php',
        'ValueError' => __DIR__ . '/..' . '/symfony/polyfill-php80/Resources/stubs/ValueError.php',
        'View' => __DIR__ . '/../..' . '/systems/Helpers/View.php',
        'Wallet' => __DIR__ . '/../..' . '/app/DesignPattern/Proxy/Wallet/Classes/Wallet.php',
        'WalletInterfaces' => __DIR__ . '/../..' . '/app/DesignPattern/Proxy/Wallet/Interfaces/WalletInterfaces.php',
        'app\\Controllers\\TestDesignController' => __DIR__ . '/../..' . '/app/Controllers/TestDesignController.php',
        'app\\Exceptions\\QueryBuilderException' => __DIR__ . '/../..' . '/app/Exceptions/QueryBuilderException.php',
        'app\\Services\\User\\Auth' => __DIR__ . '/../..' . '/app/Services/User/Auth.php',
        'systems\\Helpers\\Event' => __DIR__ . '/../..' . '/systems/Helpers/Event.php',
        'systems\\Helpers\\Helper' => __DIR__ . '/../..' . '/systems/Helpers/Helper.php',
        'systems\\Helpers\\QueryBuilder' => __DIR__ . '/../..' . '/systems/Helpers/QueryBuilder.php',
        'systems\\Helpers\\Security' => __DIR__ . '/../..' . '/systems/Helpers/Security.php',
        'systems\\Helpers\\Validation' => __DIR__ . '/../..' . '/systems/Helpers/Validation.php',
        'valid\\Event' => __DIR__ . '/..' . '/validation/user/valid/Event.php',
        'valid\\HandelError' => __DIR__ . '/..' . '/validation/user/valid/HandelError.php',
        'valid\\Helper' => __DIR__ . '/..' . '/validation/user/valid/Helper.php',
        'valid\\LoginFactory' => __DIR__ . '/..' . '/validation/user/valid/LoginFactory.php',
        'valid\\ManagerEr' => __DIR__ . '/..' . '/validation/user/valid/ManagerEr.php',
        'valid\\Security' => __DIR__ . '/..' . '/validation/user/valid/Security.php',
        'valid\\UserSecurity' => __DIR__ . '/..' . '/validation/user/valid/UserSecurity.php',
        'valid\\UserValidation' => __DIR__ . '/..' . '/validation/user/valid/UserValidation.php',
        'valid\\ValidLogin' => __DIR__ . '/..' . '/validation/user/valid/CheckValid.php',
        'valid\\ValidRegister' => __DIR__ . '/..' . '/validation/user/valid/CheckValid.php',
        'valid\\ValidationUser' => __DIR__ . '/..' . '/validation/user/valid/CheckValid.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit90d752933af3bfa76cf3069a75a11f60::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit90d752933af3bfa76cf3069a75a11f60::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInit90d752933af3bfa76cf3069a75a11f60::$prefixesPsr0;
            $loader->classMap = ComposerStaticInit90d752933af3bfa76cf3069a75a11f60::$classMap;

        }, null, ClassLoader::class);
    }
}
