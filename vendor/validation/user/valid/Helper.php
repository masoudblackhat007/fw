<?php

namespace valid;

trait Helper
{
    function getConfig($index = 'base_url'){
        $config = include "configs.php";
        return $config[$index];
    }

    public function getArrayValuesRecursively(array $array): array{
        $values = [];
        foreach ($array as $value) {
            if (is_array($value)) {
                $values = array_merge($values,
                    $this->getArrayValuesRecursively($value));
            } else {
                $values[] = $value;
            }
        }
        return $values;
    }

    function sessionCheck($key):bool{

        if (session_status() === PHP_SESSION_NONE) session_start();
        if (isset($_SESSION[$key])) return true;
        return false;
    }

    function sessionStart(){
        if (session_status() === PHP_SESSION_NONE) session_start();
    }

    public function const(){
        define("EXIST_NAME", "4041");
        define("EXIST_PASSWORD", "4042");
        define("MAX_PASSWORD", "240052");
        define("REQUIRED_PASSWOPRD", "2042");
        define("REQUIRED_NAME", "2041");
        define("ATTEMPT_SECURITY", "4001");
        define("CSRF_SECURITY", "4002");
        define("EXIST_SESSION_CSRF", "4003");
        define("EXIST_SESSION_DECAY_ATTEMPT_TIME", "4004");
        define("BLOCK_SECURITY", "4005");
        define("REQUEST_METHOD_GET", "GET");
    }

    public function block_user_per_time($time){
        // TODO: Implement blockUserPerTime() method.
        if (!isset($_SESSION['block_time_status'])){
            $_SESSION['decay_attempt_time'] = time() + $time;
            $_SESSION['block_time_status'] = true;
        }
    }

    public  function attempt($countMax): bool{
        $this->sessionStart();
        if ($this->sessionCheck('attempt')) $_SESSION['attempt'] +=1;
        else $_SESSION['attempt'] = 1;
        if ($_SESSION['attempt'] >= $countMax){
            return true;
        }
        return  false;
    }

    public function decayUserPerTime(): bool{
        if (time() >= $_SESSION['decay_attempt_time']){
            unset($_SESSION['attempt'],$_SESSION['block_time_status']);
            return true;
        }
        return false;
    }

    public function csrf_token(): string{
        // TODO: Implement csrfToken() method.
        $this->sessionStart();
        $string = base64_encode(uniqid(rand(),true));
        return $_SESSION[$_SERVER['REMOTE_ADDR']]['csrf_token'] = md5($string);
    }

    function clientCode(ValidationUser $validation, array $request, int $max_attempt = 20, $timeBlock = 5):string{
        return $validation->valid($request, $max_attempt, $timeBlock);
    }

    public function checkBlock(): bool{
        $this->sessionStart();
        if (isset($_SESSION['blockUser']) && $_SESSION['blockUser']){
            return $this->decayUserPerTime();
        }
        return true;

    }

    public function get_validation($request): Event{
        return $this->factory->user_validation($request);

    }

    public function get_security($request): Security{
        return $this->factory->user_security($request);
    }
}