<?php

namespace valid;

class HandelError implements ManagerEr
{

    private array $errors;
    use Helper;

    public function __construct()
    {
        $this->const();
    }

    public function handel(array $errors): ManagerEr{
        // TODO: Implement handel() method.

        foreach ($errors as $value) {
            foreach ($value as $element => $item) {
                foreach ($item as $key => $val) {
                    if ($val) $this->errors[$element.ucfirst("_".$key)] = $this->getConfig($element . "." . $key);
                }
            }
        }
        return $this;
    }

    /**Function index
     * @param    [viewName, data]
     *
     **/
    public function view($request){
        // TODO: Implement view() method.
        if (!empty($this->errors)) {
            foreach ($this->errors as $key => $value) {
                return $this->getConfig($key);
            }
        }
        return "ok";
    }


}
