<?php

namespace valid;

interface Event
{
//https://refactoring.guru/design-patterns/abstract-factory/php/example#example-1
    public function valid_field_name():Event;

    public function valid_field_password():Event;

    public function valid_field_confirm_password():Event;

    public function required_name():Event;

    public function required_password():Event;

    public function max_password():Event;

    public function required_confirm_password():Event;

    public function equal_password():Event;

    public function valid_field_email():Event;

    public function required_email():Event;

    public function validate_email():Event;

    public function get_errors():array ;

}