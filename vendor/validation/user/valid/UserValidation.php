<?php

namespace valid;

class UserValidation implements Event
{
    protected array $request;
    protected array $errors = [];
    public function __construct(array $request)
    {
        $this->request = $request;
    }

    public function valid_field_name(): Event{
        // TODO: Implement valid_field_name() method.
         $this->errors['exist']['name'] = !isset($this->request['name']) ?? true;
         return $this;
    }


    public function valid_field_password(): Event{
        // TODO: Implement valid_field_password() method.
         $this->errors['exist']['password'] = !isset($this->request['password']) ?? true;
         return $this;
    }

    public function required_name(): Event{
        // TODO: Implement required_name() method.
        if ($this->valid_field_name()){
            $this->errors['required']['name'] = empty($this->request['name']) ?? true;
        }

         return $this;
    }

    public function required_password(): Event{
        // TODO: Implement required_password() method.
        if ($this->valid_field_password())
            $this->errors['required']['password'] = empty($this->request['password']) ?? true;
        return $this;
    }

    public function max_password(): Event{
        // TODO: Implement max_password() method.
        if ($this->valid_field_password())
            $this->errors['max']['password'] = !(strlen($this->request['password']) >= 8) ?? true;
        return $this;
    }


    public function valid_field_confirm_password(): Event{
        // TODO: Implement valid_field_confirm_password() method.
        $this->errors['exist']['confirm_password'] = !isset($this->request['confirm_password']) ?? true;
        return $this;
    }

    public function required_confirm_password(): Event{
        // TODO: Implement confirm_password() method.
        if ($this->valid_field_confirm_password())
            $this->errors['required']['confirm_password'] = empty($this->request['confirm_password']) ?? true;
        return $this;
    }

    public function equal_password():Event{
        // TODO: Implement equal_password() method.
        if ($this->valid_field_confirm_password() && $this->valid_field_password())
            $this->errors['equal']['password'] = ($this->request['confirm_password'] !== $this->request['password']) ?? true;
        return $this;
    }

    public function valid_field_email(): Event{
        // TODO: Implement valid_field_email() method.
        $this->errors['exist']['email'] = !isset($this->request['email']) ?? true;
        return $this;
    }

    public function required_email(): Event{
        // TODO: Implement required_email() method.
        if ($this->valid_field_password())
            $this->errors['required']['email'] = empty($this->request['email']) ?? true;
        return $this;
    }

    public function validate_email(): Event{
        // TODO: Implement is_valid_email() method.
        $regex = "/^([a-zA-Z0-9\.]+@+[a-zA-Z]+(\.)+[a-zA-Z]{2,3})$/";
        $this->errors['valid']['email'] = !preg_match($regex, $this->request['email']);
        return $this;
    }

    public function get_errors(): array {
        // TODO: Implement get_errors() method.
        return $this->errors;

    }

}