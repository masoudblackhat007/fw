﻿<!doctype html>
<html class="no-js " lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

    <title>:: Aero Bootstrap4 Admin :: Sign Up</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- Custom Css -->
    <link rel="stylesheet" href=" <?php echo route(getConfig('aero')) ?>/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href=" <?php echo route(getConfig('aero')) ?>/css/style.min.css">
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>

<body class="theme-blush">

<div class="authentication">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-12">
                <form class="card auth_form" id="register" action="<?php echo route('register'); ?>" method="post">

                    <div class="header">
                        <img class="logo" src="<?php echo route(getConfig('aero')); ?>/images/logo.svg" alt="">
                        <h5>ثبت نام</h5>
                        <span>عضویت جدید را ثبت کنید</span>
                    </div>
                    <div class="body">
                        <div class="input-group mb-3">
                            <input type="hidden" id="csrf" name="csrf" value="<?php echo $csrf; ?>">
                        </div>
                        <div class="input-group mb-3">
                            <input type="text" name="name" class="form-control" placeholder="نام کاربری">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="zmdi zmdi-account-circle"></i></span>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="text" name="email" class="form-control" placeholder="ایمیل را وارد کنید">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="zmdi zmdi-email"></i></span>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="text" name="first_name" class="form-control" placeholder="نام را وارد کنید">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="zmdi zmdi-info"></i></span>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="text" name="last_name" class="form-control"
                                   placeholder="نام خانوادگی را وارد کنید">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="zmdi zmdi-info"></i></span>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="password" name="password" class="form-control" placeholder="رمزعبور">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="zmdi zmdi-lock"></i></span>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="password" name="confirm_password" class="form-control"
                                   placeholder="تکرار رمز عبور">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="zmdi zmdi-lock"></i></span>
                            </div>
                        </div>
                        <div class="checkbox">
                            <input id="remember_me" type="checkbox">
                            <label for="remember_me"> من خواندن و موافقت <a href="javascript:void(0);">با شرایط
                                    استفاده</a></label>
                        </div>
                        <button type="submit" name="submit" id="button"
                                class="btn btn-primary btn-block waves-effect waves-light">ثبت نام
                        </button>
                        <div class="signin_with mt-3">
                            <a class="link" href="sign-in.blade.php">شما قبلا عضویت دارید؟</a>
                        </div>
                    </div>
                </form>
                <div class="copyright text-center">
                    &copy;
                    <script>document.write(new Date().getFullYear())</script>
                    ,
                    <span>راست چین شده توسط <a href="https://thememakker.com/" target="_blank">آرش خادملو</a></span>
                </div>
            </div>
            <div class="col-lg-8 col-sm-12">
                <div class="card">
                    <img src=" <?php echo route(getConfig('aero')) ?>/images/signup.svg" alt="Sign Up"/>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Jquery Core Js -->
<script src=" <?php echo route(getConfig('aero')) ?>/bundles/libscripts.bundle.js"></script>
<script src=" <?php echo route(getConfig('aero')) ?>/bundles/vendorscripts.bundle.js"></script>
<!-- Lib Scripts Plugin Js -->
<script type="text/javascript">
    $(document).ready(function () {
        <?php sessionStart(); ?>
        document.getElementById('csrf').value = "<?php echo $csrf; ?>"
        const frm = $('#register');
        frm.submit(function (e) {
            e.preventDefault();
            $.ajax({
                type: "post",
                url: frm.attr('action'),
                data: frm.serialize(),
                success: function (data) {
                    var obj = JSON.parse(data);
                    if (obj.status) {
                        window.location.href = '<?php echo getConfig(); ?>' + 'admin';
                    } else {
                        <?php $_SESSION['USERID'] = false; ?>
                        alert2("خطا", obj.message);
                    }

                },
                error: function (data) {
                    alert(data);
                    console.log('An error occurred.');
                    console.log(data);
                },
            });
        });

        function alert2(title, message) {
            swal({
                title: title,
                text: message,
                icon: "error",
                buttons: "Ok",
                dangerMode: true,
            });
        }
    });
</script>
</body>

</html>