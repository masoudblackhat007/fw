<?php

return [
    //labels
    'user' =>'نام کاربری:',
    'password'=>'رمز عبور:',

    //errors
    'username.required' => "نام کاربری اجباری هست.",
    'password.required' => 'رمز عبور اجباری هست',
    'password.max' => 'طول رمز عبور مجاز نمی باشد',
    'user.found' => 'کاربری با مشخصات وارد شده وجود ندارد لطفا ثبت نام کنید',
];