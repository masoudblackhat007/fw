<?php

return [
    //labels
    'user' => 'username',
    'password' => 'password',
    //errors
    'username.required' => "Username is mandatory",
    'password.required' => 'Password is required',
    'password.max' => 'Password length is not allowed',
    'user.found' => 'There is no user with the entered details, please register',
];