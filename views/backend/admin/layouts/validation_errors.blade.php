@if (!empty($errors))
    @foreach($errors as $error)
        @foreach($error as $rule=>$message)
            <div class="alert alert-danger">
                {{ $error[$rule] }}
            </div>
        @endforeach

    @endforeach
@endif