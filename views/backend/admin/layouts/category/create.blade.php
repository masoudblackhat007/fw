<!-- Advanced Select -->
{{--<form action="" method="post">--}}
{{--    <div class="row clearfix">--}}
{{--        <div class="col-lg-12 col-md-12 col-sm-12">--}}
{{--            <div class="card">--}}
{{--                <div class="header">--}}
{{--                    <h2><strong>انتخاب</strong> پیشرفته</h2>--}}
{{--                    <ul class="header-dropdown">--}}
{{--                        <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"--}}
{{--                                                role="button" aria-haspopup="true" aria-expanded="false"> <i--}}
{{--                                        class="zmdi zmdi-more"></i> </a>--}}
{{--                            <ul class="dropdown-menu dropdown-menu-right slideUp">--}}
{{--                                <li><a href="javascript:void(0);">ویرایش</a></li>--}}
{{--                                <li><a href="javascript:void(0);">حذف</a></li>--}}
{{--                                <li><a href="javascript:void(0);">گزارش</a></li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                        <li class="remove">--}}
{{--                            <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--                <div class="body ">--}}
{{--                    <h2 class="card-inside-title">دسته بندی</h2>--}}
{{--                    <div class="col-sm-12">--}}
{{--                        <div class="form-group">--}}
{{--                            <input type="text" name="title" class="form-control" placeholder="عنوان">--}}
{{--                        </div>--}}

{{--                        <div class="form-group">--}}
{{--                            <input type="text" name="name" class="form-control" placeholder="نام دسته بندی">--}}
{{--                        </div>--}}

{{--                        <div class="form-group">--}}
{{--                            <div class="form-line">--}}
{{--                            <textarea rows="4" name="description" class="form-control no-resize"--}}
{{--                                      placeholder="لطفا توضیحات را تایپ کنید..."></textarea>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <button type="submit" class="btn btn-info">Clicked</button>--}}
{{--                </div>--}}


{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</form>--}}

<form>
    <div class="form-group">
        <label for="title">Email address</label>
        <input type="text" class="form-control" name="title" id="title" placeholder="عنوان">

    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <div class="form-line">
                            <textarea rows="4" name="description" class="form-control no-resize"
                                      placeholder="لطفا توضیحات را تایپ کنید..."></textarea>
            @if(empty($_SESSION['VALIDATION_ERROR']))
                <div class="alert alert-warning">{!! $_SESSION['VALIDATION_ERROR']['name']['required'] !!}</div>
            @endif
        </div>
    </div>
    <div class="form-group form-check">
        <input type="text" name="name" class="form-control" placeholder="نام دسته بندی">
        <label class="form-check-label" for="exampleCheck1">Check me out</label>
    </div>
    <div class="from-group">
        @if(!empty($_SESSION['EXCEPTION_ERROR_MESSAGE']))
            <div class="alert alert-warning">{{$_SESSION['EXCEPTION_ERROR_MESSAGE']}}</div>
        @endif
        @php
            unset($_SESSION['VALIDATION_ERROR'],
                    $_SESSION['EXCEPTION_ERROR_MESSAGE'],$_SESSION['SUCCESS_MESSAGE']);
        @endphp
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
