<?php

namespace systems\Helpers;

class Event
{
    public function blockUaserPerTime($time)
    {
        if (!isset($_SESSION['block_time_status'])) {
            $_SESSION['decay_attempt_time'] = time() + $time;
            $_SESSION['block_time_status'] = true;
        }
    }

    public function decayUserPerTime($checkIndex = null, $unsetIndex = null): bool
    {
        if (time() >= $_SESSION['decay_attempt_time']) {
//            exit($unsetIndex);
            unset($_SESSION['attempt'], $_SESSION['block_time_status']);
            return true;
        }
        return false;
    }
}
