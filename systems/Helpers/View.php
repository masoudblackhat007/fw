<?php

use Jenssegers\Blade\Blade;

class View
{
    public function blade(): Blade
    {
        $views = getConfig('views');
        return new Blade($views['directory'], $views['cache']);
    }
}
