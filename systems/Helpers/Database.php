<?php

class Database
{
    public function mysqliConnection()
    {


        $servername = "localhost";
        $username = "root";
        $password = "";
        // Method 1:
        $driver = new mysqli_driver();
        $driver->report_mode = MYSQLI_REPORT_STRICT | MYSQLI_REPORT_ERROR;
// OR Method 2:
//        mysqli_report(MYSQLI_REPORT_STRICT | MYSQLI_REPORT_ALL);
        try {
            $conn = new mysqli($servername, $username, $password);
            echo "Connected successfully";
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public static function pdoOpen()
    {
        $pdoAccess = (object) getConfig('pdo');
        try {
            $conn = new PDO("mysql:host=$pdoAccess->servername;dbname=$pdoAccess->database", $pdoAccess->username, $pdoAccess->password);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $conn;
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public static function pdoSelect($table, $condition)
    {
        $connection = self::pdoOpen();
        $query = $connection->prepare("select *from $table where $condition");
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
//INSERT INTO table_name (column1, column2, column3,...)
//            VALUES (value1, value2, value3,...)
    public function pdoInsert($table, $data)
    {
        $fields = '';
        $values = '';
        if (is_array($data) && !empty($data)) {
            foreach ($data as $field => $value) {
                $fields .= $field . ',';
                $values .= "'$value',";
            }
            $fields = substr($fields, 0, -1);
            $values = substr($values, 0, -1);
            $connection = $this->pdoOpen();
            $query = $connection->prepare("INSERT INTO  $table ($fields) VALUES (
            )");
            return $query->execute();
        } else {
            die("data : Syntax error");
        }
    }

    /*
     *  UPDATE table_name
     *  SET column1=value, column2=value2,...
     *  WHERE some_column=some_value
     */
    public function pdoUpdate($table, $data, $condition)
    {
        $fields = '';
        $values = '';

        foreach ($data as $field => $value) {
            $fields .= $field . '=' . "'$value',";
        }

        $fields = substr($fields, 0, -1);
        $connection = $this->pdoOpen();
        $query = $connection->prepare("UPDATE $table SET $fields WHERE $condition");
        return $query->execute();
    }

    /*
     * DELETE FROM table_name
     * WHERE some_column = some_value
     */

    public function pdoDelete($table, $condition)
    {
        $connection = $this->pdoOpen();
        $query = $connection->prepare("DELETE  FROM $table WHERE $condition");
        return $query->execute();
    }
}
