<?php

use systems\Helpers\Event;
use systems\Helpers\Response;
use systems\Helpers\Security;
use systems\Helpers\Validation;
use valid\CheckValid;
use valid\LoginFactory;

class Controllers
{
//    public function valid(){
//        return new CheckValid( new LoginFactory());
//    }

    /**
     * @return Validation
     */
    public function validation(): Validation
    {
        return new Validation();
    }

    /**
     * @return Security
     */
    public function security(): Security
    {
        return new Security();
    }

    public function view(): View
    {
        return new View();
    }

    /**Function __construct
     * @param    [null]
     * @return   QueryBuilderDB
     **/
    public function queryBuilderDB(): QueryBuilderDB
    {
        return new QueryBuilderDB();
    }

    public function loadModel($model)
    {
        return new $model();
    }

    public function event()
    {
        return new Event();
    }

    public function middleware(array $middlewareNames = null)
    {
        foreach ($middlewareNames as $middlewareName) {
            $eachMiddleware = new $middlewareName();
            $eachMiddleware->boot();
        }
        $middleware = new $middlewareName();
        $middleware->boot();
    }

    public function request($request)
    {
        $request = new $request();
        return $request->boot(request());
    }
}
