<?php

use Requtize\QueryBuilder\Connection;
use Requtize\QueryBuilder\ConnectionAdapters\PdoBridge;
use Requtize\QueryBuilder\QueryBuilder\QueryBuilderFactory;
use systems\Helpers\Helper;

/**Class QueryBuilderDB
 * @author  name: Masoud Ardi email: "masoudblackhat007@gmail.com"
 * @builder system\helpers
 * use trait Helper
 **/
class QueryBuilderDB
{
    use Helper;

   /**Function Construct
       * @param    []
       * @return   QueryBuilderFactory
   **/
    public function openQB(): QueryBuilderFactory
    {
        $pdo = new PDO(
            'mysql:dbname='
            . self::getValueInConfig('pdo', 'dbname')
            . ';host=' . self::getValueInConfig('pdo', 'host'),
            'root',
            ''
        );
        $conn = new Connection(new PdoBridge($pdo));
        return new QueryBuilderFactory($conn);
    }

    public function insert($table = "", $data = [])
    {
        try {
            $qbf = self::openQB();
            if (is_array($data) && !empty($data) && !empty($table)) {
                $qbf->from($table)->insert($data);
            }
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

    public function update($table, $data, $condition, $conditionValue)
    {
        $qbf = self::openQB();
        $qbf->from($table)->where($condition, $conditionValue)->update($data);
    }

    public function delete($table, $condition, $conditionValue)
    {
        $qbf = self::openQB();
        $qbf->from($table)->where($condition, $conditionValue)->delete();
    }

    public function select($table, $data)
    {
        $qbf = self::openQB();
        return  $qbf->from($table)->select($data);
    }
}
