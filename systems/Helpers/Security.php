<?php

namespace systems\Helpers;

use Illuminate\Support\Str;

class Security
{
    public function attempt($countMax = 3): bool
    {
        sessionStart();
        if (sessionCheck('attempt')) {
            $_SESSION['attempt'] += 1;
        } else {
            $_SESSION['attempt'] = 1;
        }
        if ($_SESSION['attempt'] >= 3) {
            return true;
        }
        return  false;
    }

    public function csrfToken(): string
    {
        return $_SESSION[$_SERVER['REMOTE_ADDR']]['csrf_token'] = md5(Str::random(64) . time());
    }

    public function checkCSRFToken($value): bool
    {
        if ($_SESSION[$_SERVER['REMOTE_ADDR']]['csrf_token'] !== $value) {
            return  false;
        }
        return  true;
    }
}
