<?php

namespace systems\Helpers;

class Validation
{
    public function check($rules, $request, $messages)
    {
        $errors = [];
        if (!empty($rules) && !empty($request) && !empty($messages)) {
            foreach ($rules as $element => $rule) {
                $elementRules = explode('|', $rule);
                foreach ($elementRules as $elementRule) {
                    if (isset($request[$element])) {
                        $filterStatus =  self::$elementRule($request[$element]);
                        if (!$filterStatus) {
                            $errors[$element][$elementRule] = $messages["$element.$elementRule"];
                        }
                    }
                }
            }
        }
        return $errors;
    }

    public function required($value): bool
    {
        return $value ?? false ;
    }

    public function min($value): bool
    {
        if (strlen($value) <= 4) {
            return false ;
        } else {
            return true;
        }
    }

    public function max($value): bool
    {
        if (strlen($value) >= 7) {
            return true;
        } else {
            return false;
        }
    }
}
