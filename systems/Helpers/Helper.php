<?php

namespace systems\Helpers;

trait Helper
{
    public function getKeyInArray($s, $a)
    {
        if (is_array($s) && is_array($a[key($s)])) {
            return $this->getKeyInArray(reset($s), $a[key($s)]);
        } else {
            if (is_array($s)) {
                $s = reset($s);
            }
            return $a[$s] ?? false;
        }
    }

    public function getValueInConfig($key1, $key2 = null)
    {
        return empty($key2) ? getConfig($key1) : $this->getKeyInArray($key2, getConfig($key1));
    }
}
