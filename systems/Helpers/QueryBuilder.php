<?php

namespace systems\Helpers;

trait QueryBuilder
{
    use Helper;

    /**Function Construct
     * @param    []
     * @return   QueryBuilderFactory
     **/
    public function openQB(): QueryBuilderFactory
    {
        try {
            $pdo = new PDO(
                'mysql:dbname='
                . self::getValueInConfig('pdo', 'dbname')
                . ';host=' . self::getValueInConfig('pdo', 'host'),
                'root',
                ''
            );
            $conn = new Connection(new PdoBridge($pdo));
            return new QueryBuilderFactory($conn);
        } catch (\Exception $exception) {
            error_log($exception->getMessage() . ".\r\n", 3, 'storage/logs/system.log');
        }
    }
}
