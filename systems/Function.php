<?php

use Jenssegers\Blade\Blade;
use systems\Helpers\Response;
use valid\CheckValid;
use valid\LoginFactory;
use valid\ValidationUser;
use valid\ValidLogin;
use valid\ValidRegister;

use function valid\clientCode;

/**Function index
 * @param    [index]
 **/
function getConfig($index = 'base_url')
{
    $config = include "configs/configs.php";
    return $config[$index];
}

/**Function index
 * @param    [dir]
 * @return   string
 **/
function route($dir): string
{
    $config = include "configs/configs.php";
    return $config['base_url'] . $dir;
}

/**Function index
 * @param    [viewName, data]
 **/
function view($viewName, string $path = '', array $data = [])
{
    if (!empty($data)) {
        foreach ($data as $val => $value) {
            $$val = $value;
        }
    }
    include "views/$path/$viewName.blade.php";
}

function redirect($router)
{
    header("Location: https://127.0.0.1/fw/$router");
}

function sessionCheck($key)
{

    if (session_status() === PHP_SESSION_NONE) {
        session_start();
    }
    if (isset($_SESSION[$key])) {
        return true;
    }
    return false;
}
function sessionStart()
{
    if (session_status() === PHP_SESSION_NONE) {
        session_start();
    }
}
/**Function index
 * @param    [errors, element]
 * @return   null
 **/
function displayError($errors, $element)
{
    if (!empty($errors)) {
        if (isset($errors[$element])) {
            foreach ($errors[$element] as $error) {
                echo '<p>' . $error . '</p>';
            }
        }
    }
}

/**Function index
 * @param    [null]
 * @return   array
 **/
function request(): array
{
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $request = $_POST ;
    }
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        $request = $_GET ;
    }
    if (!empty($_FILES)) {
        $request["file"] = $_FILES;
    }
    return $request;
}
/**Function __construct
        * @todo Prevent the robot from login in
        *@param  $captcha : Get code captcha of google recaptcha
        *@return bool     : Prevent the entry of robots
**/
function checkRecaptcha($captcha): bool
{
    if (isset($captcha)) {
        $config = include 'configs/configs.php';
        $secretKey = $config['recaptcha_google']['SECRET_KEY'];
        $recaptchaUrl = $config['recaptcha_google']['URL'];
//        $ip = $_SERVER['REMOTE_ADDR'];
        $url = $recaptchaUrl . urlencode($secretKey) .  '&response=' . urlencode($captcha);
        $response = file_get_contents($url);
        $responseKeys = json_decode($response, true);
        return $responseKeys["success"];
    } else {
        return false;
    }
}

function csrf()
{
    sessionStart();
    return $_SESSION['token'] = md5(uniqid());
//  return $_SESSION['token'] = bin2hex(random_bytes(32));
}

function checkCsrf()
{
    return isset($_SESSION['token']);
}
function lang($langName, $languageNames = ['home'])
{
    $allLanguages = [];
    if (!empty(is_dir("views/language/$langName"))) {
        if (!empty($languageNames)) {
            foreach ($languageNames as $languageName) {
                $langFile = "views/language/$langName/$languageName.php";
                if (file_exists($langFile)) {
                    $allLanguages[$languageName] = include $langFile;
                }
            }

            return $allLanguages;
        }
    } else {
        exit("$langName does not exist");
    }
}

function message($message, $title)
{
    include "views/alert.blade.php";
}

function validation_login(array $request, int $max_attempt, int $timeBlock): string
{
    $validLogin = new ValidLogin(new LoginFactory());
    return clientCode($validLogin, $request, $max_attempt, $timeBlock);
}
function validation_register(array $request, int $max_attempt, int $timeBlock): string
{
    $validLogin = new ValidRegister(new LoginFactory());
    return clientCode($validLogin, $request, $max_attempt, $timeBlock);
}
function get_block(): bool
{
    sessionStart();
    if (isset($_SESSION['block']) && $_SESSION['block']) {
        return true;
    }
    return false;
//    https://www.youtube.com/watch?v=dLPAl-7ipxY&list=PL3Y-E4YSE4wadKxqzZQJOSJPphMwZfzQH&index=4
}

function __($lang)
{
    $language = explode('.', $lang);
    $lang = current($language);
    $index = end($language);
    $langFile = "views/language/{$lang}.php";
    if (file_exists($langFile)) {
        $allLanguages = include $langFile;
        return $allLanguages[$index];
    }
}
