<?php

if (!defined('ACCESS')) {
    header("Location: https://127.0.0.1/fw/");
}
return[
    'login_method' => new OtpLogin(),
    'base_url' => "https://127.0.0.1/fw/",
    'default_controller' => 'HomeController',
    'default_method' => 'index',
    'aero' => 'public/backend/html/assets/',
    'default_language' => 'fa',
    'pdo' => [
        'servername' => 'localhost',
        'username'   => 'root',
        'password'   => '',
        'dbname'   => 'eshop',
        'host'       => '127.0.0.1'
    ],
    'views' => [
        'directory' => 'views',
        'cache' => 'cache'
    ],
    'views_errors' => [
            'directory' => 'views/backend/admin/errors',
            'cache' => 'cache'
        ],
    'recaptcha_google' => [
        'SITE_KEY' => "6LfGPjoeAAAAAF1YjtZ1mCdVAQI81b677qdVSyiW",
        'SECRET_KEY' => "6LfGPjoeAAAAAE8x13yAkQO1HZUQgNKSpUlX_6rg",
        'URL' => 'https://www.google.com/recaptcha/api/siteverify?secret='
    ]

];
