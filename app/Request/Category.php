<?php

use systems\Helpers\Validation;

class Category extends Validation
{
    public function rules(): array
    {
        return[
            'name' => 'required',
            'description' => 'required'
        ];
    }

    public function message()
    {
        return [
            'name.required' => __('name.required'),
            'description.required' => __('description.required')
        ];
    }

    public function boot($request)
    {
        $_SESSION['VALIDATION_ERROR'] = $this->check($this->rules(), $request, $this->message());
        if (!empty($_SESSION['VALIDATION_ERROR'])) {
//            unset($_SESSION['EXCEPTION_ERROR_MESSAGE'], $_SESSION['SUCCESS_MESSAGE']);
            return $_SESSION['VALIDATION_ERROR'];
        }
    }
}
