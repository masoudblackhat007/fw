<?php



class User extends QueryBuilderDB
{
    private $table = 'users';
    private $fillableStatus = true;
    private $fillable = ["id" => 'id','name' => 'name','password' => 'password'];

    public function __construct()
    {
        if (!$this->fillableStatus) {
            $this->fillable = ['*'];
        }
    }

    public function get($conditions = [])
    {
        try {
            $db = $this->openQB();
            $db = $db->from('users');
            $db->select($this->fillable);
            if (!empty($conditions)) {
                foreach ($conditions as $element => $value) {
                    if (!empty($value) && isset($this->fillable[$element])) {
                        $db->where($element, $value);
                    }
                }
            }
            return $db->all();
        } catch (Exception $e) {
            return ['code' => $e->getCode(),'message' => $e->getMessage()];
        }
    }

    public function create($data = [])
    {
        try {
            $db = $this->openQB();
            $db = $db->from($this->table);
            $inserted = $db->insert($data);
            return $inserted;
        } catch (PDOException $e) {
            return "error";
        }
    }
}
