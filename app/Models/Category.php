<?php



class Category extends QueryBuilderDB
{
    private $table = 'categories';
    private $fillableStatus = true;
    private $fillable = ["id" => 'id','name' => 'name','description' => 'description'];

    public function __construct()
    {
        if (!$this->fillableStatus) {
            $this->fillable = ['*'];
        }
    }

    public function get($conditions = [])
    {
        try {
            $db = $this->openQB();
            $db = $db->from($this->table);
            $db->select($this->fillable);
            if (!empty($conditions)) {
                foreach ($conditions as $element => $value) {
                    if (!empty($value) && isset($this->fillable[$element])) {
                        $db->where($element, $value);
                    }
                }
            }
            return $db->all();
        } catch (Exception $e) {
            return ['code' => $e->getCode(),'message' => $e->getMessage()];
        }
    }

    public function create($data = [])
    {
        try {
            $db = $this->openQB();
            $db = $db->from($this->table);
            $inserted = $db->insert($data);
            return $inserted;
        } catch (PDOException $e) {
            return "error";
        }
    }
}
