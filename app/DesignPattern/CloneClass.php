<?php

namespace App\DesignPattern;

class CloneClass
{
    private mixed $num1;
    private mixed $num2;
    public function __construct()
    {
        $this->num1 = 1;
        $this->num2 = 2;
    }

    public function inc(): void
    {
        $this->num1++;
        $this->num2++;
    }

    public function __clone(): void
    {
        $this->num1 = 1;
        $this->num2 = 2;
    }

    public function display(): array
    {
        return ([
           'num1' => $this->num1,
           'num2' => $this->num2,
        ]);
    }
}
