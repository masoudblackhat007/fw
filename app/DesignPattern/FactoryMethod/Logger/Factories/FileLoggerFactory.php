<?php

namespace App\DesignPattern\FactoryMethod\Logger\Factories;

use App\DesignPattern\FactoryMethod\Logger\Classes\FileLogger;
use App\DesignPattern\FactoryMethod\Logger\Classes\StdOutLogger;
use App\DesignPattern\FactoryMethod\Logger\Interface\LoggerInterface;

class FileLoggerFactory
{
    public function __construct(private string $message)
    {
    }

    public function createLoggerInterface(): LoggerInterface
    {
        return new FileLogger($this->message);
    }
}
