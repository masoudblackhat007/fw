<?php

namespace App\DesignPattern\FactoryMethod\Logger\Factories;

use App\DesignPattern\FactoryMethod\Logger\Classes\StdOutLogger;
use App\DesignPattern\FactoryMethod\Logger\Interface\LoggerInterface;

class StdOutLoggerFactory
{

    public function createLoggerInterface(): LoggerInterface
    {
        return new StdOutLogger();
    }
}
