<?php

namespace App\DesignPattern\FactoryMethod\Logger\Interface;

interface LoggerInterface
{
    public function log(string $message);
}