<?php

namespace App\DesignPattern\FactoryMethod\Logger\Interface;

interface LoggerFactoryInterface
{
    public function createlogger(): LoggerInterface;
}
