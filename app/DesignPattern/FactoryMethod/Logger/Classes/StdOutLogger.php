<?php

namespace App\DesignPattern\FactoryMethod\Logger\Classes;

use App\DesignPattern\FactoryMethod\Logger\Interface\LoggerInterface;

class StdOutLogger implements LoggerInterface
{

    public function log(string $message)
    {
        echo $message;
    }
}