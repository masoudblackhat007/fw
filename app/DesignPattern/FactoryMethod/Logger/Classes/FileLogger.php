<?php

namespace App\DesignPattern\FactoryMethod\Logger\Classes;

use App\DesignPattern\FactoryMethod\Logger\Interface\LoggerInterface;

class FileLogger implements LoggerInterface
{
    public function __construct(private string $filePath)
    {
    }

    public function log(string $message)
    {
        file_put_contents($this->filePath, $message, PHP_EOL, FILE_APPEND);
    }
}
