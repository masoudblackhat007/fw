<?php

namespace App\DesignPattern\FactoryMethod\Logger\Classes;

use App\DesignPattern\FactoryMethod\Logger\Interface\LoggerInterface;

class StdOutLoggerWithParameter implements LoggerInterface
{

    public function log(string $message)
    {
        echo $message;
    }
}