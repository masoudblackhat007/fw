<?php

namespace App\DesignPattern\Prototype\Book\Classes;

use App\DesignPattern\Prototype\Book\BookPrototype;

class BarBookPrototype extends BookPrototype
{
    protected string $category = 'Bar';

    public function __clone()
    {
    }
}