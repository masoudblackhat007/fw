<?php

namespace App\DesignPattern\Prototype\Book\Classes;

use App\DesignPattern\Prototype\Book\BookPrototype;

class FooBookPrototype extends BookPrototype
{
    protected string $category = 'Foo';

    public function __clone()
    {
    }
}