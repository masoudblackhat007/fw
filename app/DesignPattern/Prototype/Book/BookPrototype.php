<?php

namespace App\DesignPattern\Prototype\Book;

abstract class BookPrototype
{
    protected string $title;
    protected string $category;

    abstract public function __clone();

    final public function getTitle(): string
    {
        return $this->title;
    }

    final public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    final public function getCategory(): string
    {
        return $this->category;
    }

    final public function setCategory(string $category): void
    {
        $this->category = $category;
    }
}