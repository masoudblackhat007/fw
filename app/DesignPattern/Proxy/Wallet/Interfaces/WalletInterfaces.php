<?php

interface WalletInterfaces
{
    public function deposit(int $amount);

    public function getBalance(): int;
}