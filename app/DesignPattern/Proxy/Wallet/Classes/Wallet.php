<?php

class Wallet implements WalletInterfaces
{
    /**
     * @var int[]
     */
    private array $transactions = [];
    private int $depositAccess = 0;

    public function deposit(int $amount)
    {
        $this->transactions[] = $amount;
    }

    public function getBalance(): int
    {
        // this is the heavy part, imagine all the transactions even from
        // years and decades ago must be fetched from a database or web service
        // and the balance must be calculated from it

        return array_sum($this->transactions);
    }

    public function setAccessToDepositMethod()
    {
        return $this->depositAccess = 1;
    }
}
