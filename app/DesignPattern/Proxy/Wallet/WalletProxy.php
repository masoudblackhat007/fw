<?php

namespace App\DesignPattern\Proxy\Wallet;

class WalletProxy extends Wallet implements WalletInterfaces
{
    private ?int $balance = null;
    private int $depositAccess = 0;

    public function getBalance(): int
    {
        // because calculating balance is so expensive,
        // the usage of BankAccount::getBalance() is delayed until it really is needed
        // and will not be calculated again for this instance
        if ($this->depositAccess === 1) {
            if ($this->balance === null) {
                $this->balance = parent::getBalance();
            }
            return $this->balance;
        }
        return 0;
    }

    public function checkDepositAccess()
    {
        if ($this->depositAccess === 0) {
            $this->depositAccess = parent::setAccessToDepositMethod();
        }
    }
}
