<?php

namespace App\DesignPattern\Proxy\Book\Classes;

use App\DesignPattern\Proxy\Book\Interfaces\BookListInterface;

class BookList implements BookListInterface
{
    private array $books = [];

    public function getBooksCount(): int
    {
        return count($this->books);
    }

    public function addBook(Book $bookIn): object
    {
        $this->books[] = $bookIn;
        return $this;
    }

    public function getBook(int $bookId): object
    {
        if (isset($this->books[$bookId])) {
            return $this->books[$bookId];
        }
        return (object)[];
    }

    public function getBooks(): array
    {
        return $this->books;
    }

    public function getBookId(): int
    {
        if (!empty($this->books)) {
            return array_key_last($this->books);
        }
        return -1;
    }
}
