<?php

namespace App\DesignPattern\Book\Proxy;

use App\DesignPattern\Proxy\Book\Classes\Book;
use App\DesignPattern\Proxy\Book\Classes\BookList;
use App\DesignPattern\Proxy\Book\Interfaces\BookListInterface;

class ProxyBookList extends BookList implements BookListInterface
{
    private $bookList = null;
    private int $bookId = 0;
    private ?array $books = null;
    private ?object $book = null;
    private ?int $booksCount = 0;

    public function getBooksCount(): int
    {
        if ($this->booksCount === 0) {
            $this->booksCount = parent::getBooksCount();
        }
        return $this->booksCount;
    }

    public function addBook(Book $bookIn): object
    {
        parent::addBook($bookIn);
        return $this;
    }

    public function getBook(int $bookId): object
    {
        if (null == $this->book) {
            $this->book = parent::getBook($bookId);
        }
        return $this->book;
    }

    public function getBooks(): array
    {
        if ($this->books === null) {
            $this->books = parent::getBooks();
        }
        return $this->books;
    }

    public function getBookId(): int
    {
        return parent::getBookId();
    }
}
