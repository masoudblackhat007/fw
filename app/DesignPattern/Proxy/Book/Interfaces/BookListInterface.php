<?php

namespace App\DesignPattern\Proxy\Book\Interfaces;

use App\DesignPattern\Proxy\Book\Classes\Book;

interface BookListInterface
{
    public function getBooksCount(): int;

    public function addBook(Book $bookIn): object;

    public function getBook(int $bookId): object;

    public function getBookId(): int;

    public function getBooks(): array;
}
