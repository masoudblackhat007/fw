<?php

namespace App\DesignPattern\Database;

use App\DesignPattern\Database\Interfaces\DatabaseInterfaces;
use App\DesignPattern\Database\Interfaces\DatabaseLogInterfaces;

class Database
{
    private object $logClass;
    private object $databaseClass;
    public function __construct(DatabaseLogInterfaces $databaseLogInterfaces, DatabaseInterfaces $databaseInterfaces)
    {
        $this->logClass = $databaseLogInterfaces;
        $this->databaseClass = $databaseInterfaces;
    }

    public function insert()
    {
        $this->databaseClass->insert();
        return $this;
    }

    public function log()
    {
        $this->logClass->log();
        return $this;
    }
}
