<?php

namespace App\DesignPattern\Database\Interfaces;

interface DatabaseInterfaces
{
    public function insert(): string;
}
