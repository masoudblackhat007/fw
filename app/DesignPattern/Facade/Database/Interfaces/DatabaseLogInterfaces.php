<?php

namespace App\DesignPattern\Database\Interfaces;

interface DatabaseLogInterfaces
{
    public function log(): string;
}
