<?php

namespace App\DesignPattern\Database\SubClasses;

use App\DesignPattern\Database\Interfaces\DatabaseInterfaces;

class DatabaseClass implements DatabaseInterfaces
{

    public function insert(): string
    {
        return "inserted";
    }
}
