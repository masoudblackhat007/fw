<?php

namespace App\DesignPattern\Database\SubClasses;

use App\DesignPattern\Database\Interfaces\DatabaseLogInterfaces;

class LogClass implements DatabaseLogInterfaces
{
    public function log(): string
    {
        return "log";
    }
}
