<?php

namespace Hash;

use Hash\Methods\Md5;
use Hash\Methods\Sh1;

class Hash
{
    private const ALGORITHMS = [
        'md5' => Md5::class,
        'sh1' => Sh1::class
    ];

    public function make(string $value, string $algorithms): string
    {
        if (!array_key_exists($algorithms, self::ALGORITHMS)) {
            throw new Exception("ALGORITHMS Not Exist", 1);
        }
        $algorithmsClass = self::ALGORITHMS[$algorithms];
        return (new $algorithmsClass())->make($value);
    }
}
