<?php

namespace Hash\Methods;

use Hash\Interfaces\HashInterfaces;

class Md5 implements HashInterfaces
{
    public function make($value = null): string
    {
        // TODO: Implement make() method.
        return md5($value);
    }
}
