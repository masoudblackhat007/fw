<?php

namespace Hash\Methods;

use Hash\Interfaces\HashInterfaces;

class Sh1 implements HashInterfaces
{
    public function make($value = null): string
    {
        // TODO: Implement make() method.
        return sha1($value);
    }
}
