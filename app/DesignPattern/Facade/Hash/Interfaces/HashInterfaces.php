<?php

namespace Hash\Interfaces;

interface HashInterfaces
{
    public function make($value): string;
}
