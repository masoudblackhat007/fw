<?php

namespace System\Interfaces;
interface SystemOperating
{
    public function getName();

    public function getFamily();
}