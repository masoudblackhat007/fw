<?php

namespace System\Interfaces;
interface SystemInterface
{
    public function start();

    public function end();
}