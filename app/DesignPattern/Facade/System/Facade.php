<?php

namespace System;

class Facade
{
    protected $subsystem;
    protected $systemOperating;

    /**
     * Depending on your application's needs, you can provide the Facade with
     * existing subsystem objects or force the Facade to create them on its own.
     */
    public function __construct(\System\Interfaces\SystemInterface $subsystem, \System\Interfaces\SystemOperating $systemOperating)
    {
        $this->subsystem = $subsystem;
        $this->systemOperating = $systemOperating;
    }

    /**
     * The Facade's methods are convenient shortcuts to the sophisticated
     * functionality of the subsystems. However, clients get only to a fraction
     * of a subsystem's capabilities.
     */
    public function operation(): string
    {
        $result = "Facade initializes subsystems:\n";
        $result .= $this->subsystem->start();
        $result .= $this->subsystem->start();
        $result .= "Facade orders subsystems to perform the action:\n";
        $result .= $this->systemOperating->getName();
        $result .= $this->systemOperating->getFamily();

        return $result;
    }
}
