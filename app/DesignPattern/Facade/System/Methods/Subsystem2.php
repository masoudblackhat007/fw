<?php

namespace System\Methods;

use System\Interfaces\SystemOperating;

class Subsystem2 implements SystemOperating
{
    public function getName(): string
    {
        return "Subsystem2: Get ready!\n";
    }

    // ...

    public function getFamily(): string
    {
        return "Subsystem2: Fire!\n";
    }
}
