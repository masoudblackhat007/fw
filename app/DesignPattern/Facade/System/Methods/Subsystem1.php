<?php

namespace System\Methods;

use System\Interfaces\SystemInterface;

class Subsystem1 implements SystemInterface
{
    public function start(): string
    {
        return "Subsystem1: Ready!\n";
    }

    // ...

    public function end(): string
    {
        return "Subsystem1: Go!\n";
    }
}