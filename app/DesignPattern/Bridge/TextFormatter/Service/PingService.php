<?php

namespace App\DesignPattern\Bridge\Service;

use App\DesignPattern\Bridge\Service;

class PingService extends Service
{
    public function get(): string
    {
        return $this->implementation->format('pong');
    }
}