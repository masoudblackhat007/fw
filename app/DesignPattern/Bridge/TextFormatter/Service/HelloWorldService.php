<?php

namespace App\DesignPattern\Bridge\Service;

use App\DesignPattern\Bridge\Service;

class HelloWorldService extends Service
{
    public function get(): string
    {
        return $this->implementation->format('Hello World');
    }
}
