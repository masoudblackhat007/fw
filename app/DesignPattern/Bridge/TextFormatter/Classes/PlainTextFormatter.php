<?php

namespace App\DesignPattern\Bridge\Classes;

use App\DesignPattern\Bridge\Interfaces\FormatterInterface;

class PlainTextFormatter implements FormatterInterface
{
    public function format(string $text): string
    {
        // TODO: Implement format() method.
    }
}
