<?php

namespace App\DesignPattern\Bridge\Classes;

use App\DesignPattern\Bridge\Interfaces\FormatterInterface;

class HtmlFormatter implements FormatterInterface
{
    public function format(string $text): string
    {
        return sprintf('<p>%s</p>', $text);
    }
}
