<?php

namespace App\DesignPattern\Bridge\Interfaces;

interface FormatterInterface
{
    public function format(string $text): string;
}
