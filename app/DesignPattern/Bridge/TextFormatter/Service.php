<?php

namespace App\DesignPattern\Bridge;

use App\DesignPattern\Bridge\Interfaces\FormatterInterface;

abstract class Service
{
    protected FormatterInterface $implementation;

    final public function setImplementation(FormatterInterface $printer)
    {
        $this->implementation = $printer;
    }

    abstract public function get(): string;
}
