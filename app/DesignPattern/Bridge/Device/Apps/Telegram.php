<?php

namespace App\DesignPattern\Bridge\Device\Apps;

use App\DesignPattern\Bridge\Device\Interfaces\MassageInterface;

class Telegram implements MassageInterface
{
    public function send($body): string
    {
        return $body;
    }
}
