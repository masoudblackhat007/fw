<?php

namespace App\DesignPattern\Bridge\Device\Devisec;

use App\DesignPattern\Bridge\Device\Device;

class Tablet extends Device
{
    public function send($body)
    {
        return  $this->sender->send($body . "send by tablet");
    }
}
