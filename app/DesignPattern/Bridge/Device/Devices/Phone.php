<?php

namespace App\DesignPattern\Bridge\Device\Devices;

use App\DesignPattern\Bridge\Device\Device;

class Phone extends Device
{
    public function send($body)
    {
        return $this->sender->send($body . " send by phone");
    }
}
