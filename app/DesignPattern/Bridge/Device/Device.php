<?php

namespace App\DesignPattern\Bridge\Device;

use App\DesignPattern\Bridge\Device\Interfaces\DeviceInterface;
use App\DesignPattern\Bridge\Device\Interfaces\MassageInterface;

abstract class Device
{
    protected MassageInterface $sender;
    public function setSender(MassageInterface $sender)
    {
        $this->sender = $sender;
    }

    abstract public function send($body);
}
