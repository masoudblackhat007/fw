<?php

namespace App\DesignPattern\Bridge\Device\Interfaces;

interface MassageInterface
{
    public function send($body): string;
}
