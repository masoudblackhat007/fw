<?php

namespace App\DesignPattern\Bridge\Device\Interfaces;

interface DeviceInterface
{
    public function setSender(MassageInterface $sender);

    public function send($body);
}
