<?php

namespace App\DesignPattern\FlyweightFactory;

use App\DesignPattern\FlyweightFactory\Classes\Character;
use App\DesignPattern\FlyweightFactory\Classes\Word;
use App\DesignPattern\FlyweightFactory\Interfaces\TextInterface;

class TextFactory implements \Countable
{
    /**
     * @var TextInterface[]
     */
    private array $charPool = [];

    public function get(string $name): TextInterface
    {
        if (!isset($this->charPool[$name])) {
            $this->charPool[$name] = $this->create($name);
        }

        return $this->charPool[$name];
    }

    private function create(string $name): TextInterface
    {
        if (strlen($name) == 1) {
            return new Character($name);
        } else {
            return new Word($name);
        }
    }

    public function count(): int
    {
        return count($this->charPool);
    }
}
