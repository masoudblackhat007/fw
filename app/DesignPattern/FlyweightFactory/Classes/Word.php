<?php

namespace App\DesignPattern\FlyweightFactory\Classes;

use App\DesignPattern\FlyweightFactory\Interfaces\TextInterface;

class Word implements TextInterface
{
    public function __construct(private string $name)
    {
    }

    public function render(string $extrinsicState): string
    {
        return sprintf('Word %s with font %s', $this->name, $extrinsicState);
    }
}