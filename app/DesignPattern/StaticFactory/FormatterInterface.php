<?php

namespace App\DesignPattern\StaticFactory;

interface FormatterInterface
{
    public function format(string $input): string;
}