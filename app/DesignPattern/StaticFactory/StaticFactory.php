<?php

namespace App\DesignPattern\StaticFactory;




use App\DesignPattern\StaticFactory\Classes\FormatNumber;
use App\DesignPattern\StaticFactory\Classes\FormatString;

final class StaticFactory
{
    public static function factory(string $type): FormatterInterface
    {
        return match ($type) {
            'number' => new FormatNumber(),
            'string' => new FormatString(),
            default => throw new InvalidArgumentException('Unknown format given'),
        };
    }
}