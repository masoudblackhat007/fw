<?php

namespace App\DesignPattern\StaticFactory\Classes;

use App\DesignPattern\StaticFactory\FormatterInterface;

class FormatNumber implements FormatterInterface
{
    public function format(string $input): string
    {
        return (number_format($input, 3, '/'));
    }
}
