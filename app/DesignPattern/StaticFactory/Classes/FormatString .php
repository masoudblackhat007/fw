<?php

namespace App\DesignPattern\StaticFactory\Classes;

use App\DesignPattern\StaticFactory\FormatterInterface;

class FormatString implements FormatterInterface
{
    public function format(string $input): string
    {
        return $input;
    }
}
