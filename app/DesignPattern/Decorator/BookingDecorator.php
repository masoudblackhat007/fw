<?php

namespace App\DesignPattern\Decorator;

use App\DesignPattern\Decorator\interfaceBooking\Booking;

abstract class BookingDecorator implements Booking
{
    public function __construct(protected Booking $booking)
    {
    }
}
