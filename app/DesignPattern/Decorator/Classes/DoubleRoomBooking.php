<?php

namespace App\DesignPattern\Decorator\Classes;

class DoubleRoomBooking
{
    public function calculatePrice(): int
    {
        return 40;
    }

    public function getDescription(): string
    {
        return 'double room';
    }
}
