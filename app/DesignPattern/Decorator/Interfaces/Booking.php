<?php

namespace App\DesignPattern\Decorator\interfaceBooking;

interface Booking
{
    public function calculatePrice(): int;
    public function getDescription(): string;
}
