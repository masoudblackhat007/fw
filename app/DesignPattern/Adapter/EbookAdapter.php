<?php

namespace App\DesignPattern\Adapter;

use App\DesignPattern\Adapter\Interfaces\EbookInterface;

class EbookAdapter
{
    protected EbookInterface $eBook;
    public function __construct(EbookInterface $eBook)
    {
        $this->eBook = $eBook;
    }

    /**
     * This class makes the proper translation from one interface to another.
     */
    public function open()
    {
        $this->eBook->unlock();
    }

    public function turnPage()
    {
        $this->eBook->pressNext();
    }

    /**
     * notice the adapted behavior here: EBook::getPage() will return two integers, but Book
     * supports only a current page getter, so we adapt the behavior here
     */
    public function getPage(): int | array
    {
        return $this->eBook->getPage();
    }
}
