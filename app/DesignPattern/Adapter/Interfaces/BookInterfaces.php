<?php

namespace App\DesignPattern\Adapter\Interfaces;

interface BookInterfaces
{
    public function turnPage();

    public function open();

    public function getPage(): int | array;
}
