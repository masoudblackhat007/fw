<?php

namespace App\DesignPattern\Adapter\Classes;

use App\DesignPattern\Adapter\Interfaces\EbookInterface;

class Kindle implements EbookInterface
{
    private int $page = 1;
    private int $totalPages = 100;
    public function pressNext()
    {
        $this->page++;
    }

    public function unlock()
    {
        $this->page = 1;
    }

    /**
     * returns current page and total number of pages, like [10, 100] is page 10 of 100
     *
     * @return int[]
     */
    public function getPage(): array
    {
        return [$this->page, $this->totalPages];
    }
}
