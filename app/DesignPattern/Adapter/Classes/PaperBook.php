<?php

namespace App\DesignPattern\Adapter\Classes;

use App\DesignPattern\Adapter\Interfaces\BookInterfaces;
use App\DesignPattern\Proxy\Book\Classes\Book;
use App\DesignPattern\Proxy\Book\Interfaces\BookListInterface;

class PaperBook implements BookInterfaces
{
    private int $page;

    public function open(): void
    {
        $this->page = 1;
    }

    public function turnPage(): void
    {
        $this->page++;
    }

    public function getPage(): int
    {
        return $this->page;
    }
}
