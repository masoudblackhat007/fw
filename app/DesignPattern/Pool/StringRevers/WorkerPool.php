<?php

namespace App\DesignPattern\Pool\StringRevers;

use App\DesignPattern\Pool\StringRevers\ConverterClass\ConverterAbstract;
use Countable;

class WorkerPool extends ConverterAbstract implements Countable
{
    /**
     * @var WorkerPoolInterface[]
     */
    private array $occupiedWorkers = [];

    /**
     * @var WorkerPoolInterface[]
     */
    private array $freeWorkers = [];
    private object $worker;

    public function getFreeWorkers(): array
    {
        return $this->freeWorkers;
    }

    public function getOccupiedWorkers(): array
    {
        return $this->occupiedWorkers;
    }

    public function get(string $object): WorkerPool
    {
        if (count($this->freeWorkers) === 0) {
            $this->worker = new $object();
        } else {
            $this->worker = array_pop($this->freeWorkers);
        }

        $this->occupiedWorkers[spl_object_hash($this->worker)] = $this->worker;

        return $this;
    }

    public function getObject(): object
    {
        return $this->worker;
    }

    public function dispose(WorkerPoolInterface $worker): void
    {
        $key = spl_object_hash($worker);
        if (isset($this->occupiedWorkers[$key])) {
            unset($this->occupiedWorkers[$key]);
            $this->freeWorkers[$key] = $worker;
        }
    }

    public function count(): int
    {
        return count($this->occupiedWorkers) + count($this->freeWorkers);
    }
}
