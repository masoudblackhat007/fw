<?php

namespace App\DesignPattern\Pool\StringRevers;

class StringReverseWorker implements WorkerPoolInterface
{
    public function run(string $text): string
    {
        return strrev($text);
    }
}