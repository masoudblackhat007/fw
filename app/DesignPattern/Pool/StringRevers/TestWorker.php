<?php

namespace App\DesignPattern\Pool\StringRevers;

class TestWorker implements WorkerPoolInterface
{
    public function test(): string
    {
        return "test";
    }
}
