<?php

namespace App\DesignPattern\Pool\StringRevers\ConverterClass;

use App\DesignPattern\Pool\StringRevers\StringReverseWorker;
use App\DesignPattern\Pool\StringRevers\TestWorker;
use MongoDB\BSON\ObjectId;

abstract class ConverterAbstract implements ConverterInterface
{
    public function getTestWork(object $object): TestWorker
    {
        return $object;
    }

    public function getStringReverseWorks(object $object): StringReverseWorker
    {
        return $object;
    }
}
