<?php

namespace App\DesignPattern\DataMapper\Mappers;

use App\DesignPattern\DataMapper\Classes\User;
use App\DesignPattern\DataMapper\StorageAdapter;
use InvalidArgumentException;

class JsonUserMapper
{
    private StorageAdapter $adapter;
    private array $result;
    public function __construct(StorageAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function mapFields($result)
    {
        $this->result = [
            'username' => $result['user'],
            'email' => $result['email_address']
        ];
    }
    /**
     * finds a user from storage based on ID and returns a User object located
     * in memory. Normally this kind of logic will be implemented using the Repository pattern.
     * However the important part is in mapRowToUser() below, that will create a business object from the
     * data fetched from storage
     */
    public function findById(int $id): JsonUser
    {
        $result = $this->adapter->find($id);
        $this->mapFields($result);
        if ($result === null) {
            throw new InvalidArgumentException("User #$id not found");
        }

        return $this->mapRowToUser($result);
    }

    private function mapRowToUser(array $row): JsonUser
    {
        return JsonUser::fromState($row);
    }
}
