<?php

namespace App\Services\User\Interfaces;

interface LoginInterface
{
    public function login();
}