<?php

namespace app\Services\User;

use Symfony\Component\Config\Loader\LoaderInterface;

class Auth
{
    private static array $instances = [];
    private array $data;
    private object $loginMethod;
//    public function __construct(LoaderInterface $loginMethod)
//    {
//        $this->data = $data;
//        $this->loginMethod = $loginMethod;
//    }

    public static function getInstance($strategy): Auth
    {
        $cls = static::class;
        if (!isset(self::$instances[$cls])) {
            self::$instances[$cls] = new static($strategy);
        }

        return self::$instances[$cls];
    }

    public function getUserId(): int
    {
        sessionStart();
        if (isset($_SESSION['USERID'])) {
            return $_SESSION['USERID'];
        }
        return 0;
    }

    public function login()
    {
        $this->loginMethod->login();
    }
}
