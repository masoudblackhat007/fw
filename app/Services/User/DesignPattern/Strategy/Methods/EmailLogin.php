<?php

namespace App\Services\User\DesignPattern\Strategy\Methods;

class EmailLogin implements LoginInterface
{
    public function login()
    {
        echo "Email Login";
    }

    public function getMethodName(): string
    {
        return 'EmailLogin';
    }
}
