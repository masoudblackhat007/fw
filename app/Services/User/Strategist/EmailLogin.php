<?php

namespace App\Services\User\Strategist;

use App\Services\User\Interfaces\LoginInterface;

class EmailLogin implements LoginInterface
{
    public function login()
    {
        echo "Email Login";
    }
}
