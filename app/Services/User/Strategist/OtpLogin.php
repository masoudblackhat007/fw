<?php

namespace App\Services\User\Strategist;

use App\Services\User\Interfaces\LoginInterface;

class OtpLogin implements LoginInterface
{

    public function login()
    {
        echo "OTP Login";
    }
}