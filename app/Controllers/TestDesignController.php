<?php

namespace app\Controllers;

use App\DesignPattern\CloneClass;
use App\DesignPattern\Database\Database;
use App\DesignPattern\Database\SubClasses\DatabaseClass;
use App\DesignPattern\Database\SubClasses\LogClass;
use App\DesignPattern\FactoryMethod\Logger\Classes\StdOutLogger;
use App\DesignPattern\FactoryMethod\Logger\Factories\StdOutLoggerFactory;
use App\DesignPattern\Pool\StringRevers\StringReverseWorker;
use App\DesignPattern\Pool\StringRevers\TestWorker;
use App\DesignPattern\Pool\StringRevers\WorkerPool;
use App\DesignPattern\Prototype\Book\Classes\BarBookPrototype;
use App\DesignPattern\Prototype\Book\Classes\FooBookPrototype;
use App\DesignPattern\StaticFactory\StaticFactory;
use Cake\Core\Configure;

class TestDesignController extends \Controllers
{
    public function test()
    {
//        $loggerFactory = new StdOutLoggerFactory();
//        $logger = $loggerFactory->createLoggerInterface();
//        $stdLogger = new StdOutLogger();
////        $type = getType($stdLogger);
//        if ($stdLogger == $logger) {
//            print_r("passed");
//        }
//
//        exit();
        $pool = new WorkerPool();
        $worker1 = $pool->get(TestWorker::class);
//        $pool->dispose($worker1);
        $worker2 = $pool->get(StringReverseWorker::class)->getStringReverseWorks($pool->getObject());
//        $main = (StringReverseWorker::class)($worker2);
//        $reflector = new \ReflectionClass(StringReverseWorker::class);

//        $test = object_get($worker2, null)->;
//        $ob = get_class($worker2);
//        if ($worker2 instanceof StringReverseWorker) {
//            print_r($pool->converter($worker2)->run("ok"));
//        }
        print_r($worker2->run("ok"));

        exit();
        $worker1 = $pool->get();
        $worker2 = $pool->get();
        $worker3 = $pool->get();
        $worker4 = $pool->get();
        echo $worker4->run("masoud ardi");
        echo "<br>";
        $pool->dispose($worker2);
        if ($pool->count() == 4) {
            echo "count passed";
        }
        echo "<pre>";
        print_r($pool->getOccupiedWorkers());
//        $this->assertCount(2, $pool);
//        $this->assertNotSame($worker1, $worker2);
        exit();
        $fooPrototype = new FooBookPrototype();
        $barPrototype = new BarBookPrototype();

        for ($i = 0; $i < 10; $i++) {
            $book = clone $fooPrototype;
            $book->setTitle('Foo Book No ' . $i);
            echo $book->getTitle();
            echo "<br>";
//            $this->assertInstanceOf(FooBookPrototype::class, $book);
        }
        echo "<hr>";
        for ($i = 0; $i < 5; $i++) {
            $book = clone $barPrototype;
            $book->setTitle('Bar Book No ' . $i);
            echo $book->getTitle();
            echo "<br>";
//            $this->assertInstanceOf(BarBookPrototype::class, $book);
        }
        exit();
        $cloneTest = new CloneClass();
        $cloneTest->inc();
        print_r($cloneTest->display());

        $copyCloneClass = clone($cloneTest);
        print_r($copyCloneClass->display());
        exit();
        $formatter = StaticFactory::factory('number');
        echo $formatter->format('1223456.134666');
//        $database = new Database(new LogClass(), new DatabaseClass());
//        $database->insert()->log();
    }
}
