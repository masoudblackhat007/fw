<?php

namespace App\Controllers;

use App\DesignPattern\Adapter\Classes\Kindle;
use App\DesignPattern\Adapter\Classes\PaperBook;
use App\DesignPattern\Adapter\EbookAdapter;
use App\DesignPattern\Bridge\Classes\HtmlFormatter;
use App\DesignPattern\Bridge\Device\Devices\Phone;
use App\DesignPattern\Bridge\Service\HelloWorldService;
use App\DesignPattern\DataMapper\Mappers\JsonUserMapper;
use App\DesignPattern\DataMapper\Mappers\UserMapper;
use App\DesignPattern\DataMapper\StorageAdapter;
use App\DesignPattern\Decorator\Classes\DoubleRoomBooking;
use App\DesignPattern\Decorator\Classes\WiFi;
use App\DesignPattern\DependencyInjection\Configs\DatabaseConfiguration;
use App\DesignPattern\DependencyInjection\DatabaseConnection;
use App\DesignPattern\Fluent\QueryBuilder\Sql;
use App\DesignPattern\FlyweightFactory\TextFactory;
use App\DesignPattern\Registry\Registry;
use App\DesignPattern\Registry\Services\Service;
use App\DesignPattern\Registry\Services\Telegram;
use Controllers;
use valid\Helper;

/**Class Controller
 * @author  name: Masoud Ardi email: "masoudblackhat007@gmail.com"
 * @builder App\Controllers
 * @extends Controllers
 **/
class UserController extends Controllers
{
    use Helper;

    private $lang;
    private $fields = ['id', 'name', 'password'];
    private $director;
    private $csrf;
    private object $service;
//    private object $authServices;

    /**Function __construct
     * @todo Define language method
     **/
    public function __construct()
    {

        $lang = getConfig('default_language');
        $this->lang = lang($lang, ['home', 'login', 'registration']);
//        $this->authServices = Auth::getInstance(getConfig('login_method'));
    }

    public function login()
    {
        $this->service = new Telegram();
        Registry::set(Registry::TELEGRAM, $this->service);
        print_r(Registry::get(Registry::TELEGRAM));
        exit();
        $characters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
            'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
        $fonts = ['Arial', 'Times New Roman', 'Verdana', 'Helvetica'];
        $factory = new TextFactory();
        for ($i = 0; $i <= 10; $i++) {
            foreach ($characters as $char) {
                foreach ($fonts as $font) {
                    $flyweight = $factory->get($char);
                    $rendered = $flyweight->render($font);

                    $this->assertSame(sprintf('Character %s with font %s', $char, $font), $rendered);
                }
            }
        }
        exit();
        $query = (new Sql())
            ->select(['foo', 'bar'])
            ->from('foobar', 'f')
            ->where('f.bar = ?');
        echo $query;
        echo "<br>";
        $query = str_replace('?', '%s', $query);
        echo sprintf($query, 1);
        exit();
        $storage = new StorageAdapter([1 => ['user' => 'domnikl'
            , 'email_address' => 'liebler.dominik@gmail.com']]);
        $mapper = new JsonUserMapper($storage);

        $user = $mapper->findById(1);

//        $storage = new StorageAdapter([1 => ['user' => 'domnikl', 'email_address' => 'liebler.dominik@gmail.com']]);
//        $mapper = new UserMapper($storage);
//
//        $user = $mapper->findById(1);

        exit();
        $device = new Phone();
        $device->setSender(new Telegram());
        echo $device->send("telegram");
        exit();
        $bridge = new HelloWorldService();
        $bridge->setImplementation(new HtmlFormatter());
        echo $bridge->get('hello world');
        exit();
        $book = new PaperBook();
        $book->open();
        $book->turnPage();
        echo $book->getPage();
        echo "<br>-------------------------------<br>";
        $kindle = new Kindle();
        $eBook = new EbookAdapter($kindle);
        $eBook->open();
        $eBook->turnPage();
        $eBook->turnPage();
        echo $eBook->getPage();
        exit();
        //        DependencyInjection
        $config = new DatabaseConfiguration('localhost', 3306, 'domnikl', '1234');
        $connection = new DatabaseConnection($config);

        echo $connection->getDsn();
        exit();
        $wallet = new WalletProxy();
        $wallet->checkDepositAccess();
        $wallet->deposit(30);
        $wallet->deposit(60);
        $wallet->deposit(20);
        echo $wallet->getBalance();
        echo $this->authServices->login();
//        proxy
        $proxyBookList = new ProxyBookList();
        echo  $proxyBookList->addBook(new Book("PHP for cats", "test1"))->getBookId();
        echo  $proxyBookList->addBook(new Book("ASP for cats", "test2"))->getBookId();
        echo  $proxyBookList->addBook(new Book("ASPX for cats", "tes3"))->getBookId();
//        echo $proxyBookList->getBooksCount();
        echo "<pre>";
        $book = $proxyBookList->getBook(1);
        print_r($book->getAuthor());
        exit();
        //Decorator
        $booking = new DoubleRoomBooking();
        echo $booking->calculatePrice();
        echo $booking->getDescription();
        $booking = new WiFi($booking);
        $booking->calculatePrice();
        $booking->getDescription();

        exit();

        $request = request();
        sessionStart();
        if ($_SERVER["REQUEST_METHOD"] === "GET") {
            if (!$this->checkBlock()) {
                redirect('block');
            }
            if (isset($_SESSION['USERID']) && $_SESSION['USERID']) {
                redirect('admin');
                exit();
            }
            echo $this->view()->blade()->render('sign-in', ['csrf' => $this->csrf_token(), 'lang' => $this->lang]);
            exit();
        } else {
            sessionStart();
            $valid = validation_login($request, 30, 15);
            if ($valid === "ok") {
                $password = $request['password'];
                unset($request['password']);
                $users = $this->loadModel(\User::class);
                $user = current($users->get($request));
                if (isset($user->name) && $request['name'] === $user->name) {
                    if (password_verify($password, $user->password)) {
                        $_SESSION['USERID'] = true;
                        echo json_encode(['code' => 200, 'message' => "success", 'status' => true]);
                    } else {
                        echo json_encode(['code' => 412, 'message' => "رمز وارد شده اشتباه است", 'status' => false]);
                    }
                } else {
                    echo json_encode(['code' => 413, 'message' => "نام کاربری اشتباه است", 'status' => false]);
                }
            } elseif ($valid === 'block') {
                echo json_encode(['code' => 800, 'message' => "success", 'status' => false]);
            } else {
                echo json_encode(['code' => 1020, 'message' => $valid, 'status' => false]);
            }
            exit();
        }
    }

    /**Function __construct
     * @todo User registration
     **/
    public function register()
    {
        sessionStart();
        $request = request();
        if ($_SERVER['REQUEST_METHOD'] === "GET") {
            if (isset($_SESSION['USERID']) && $_SESSION['USERID'] > 0) {
                redirect('admin');
            }
            if (!$this->checkBlock()) {
                redirect('block');
            }
            echo $this->view()->blade()->render('sign-up', ['csrf' => $this->csrf_token(), 'lang' => $this->lang]);
            exit();
        } else {
            sessionStart();
            $valid = validation_register($request, 40, 4);
            if ($valid === "ok") {
                $DB = [];
                $DB['name'] = $request['name'];
                $DB['password'] = password_hash($request['password'], PASSWORD_DEFAULT);
                $DB['first_name'] = $request['first_name'];
                $DB['last_name'] = $request['last_name'];
                $DB['email'] = $request['email'];
                $users = $this->loadModel(\User::class);
                $user = $users->create($DB);
                if (str_contains($user, '1062')) {
                    echo json_encode(['code' => 432
                        , 'message' => "شما قبلا ثبت نام کرده اید لطفا وارد شوید", 'status' => false]);
                } else {
                    $_SESSION['USERID'] = $user;
                    echo json_encode(['code' => 200, 'message' => "شما با موفقیت ثبت نام شدید", 'status' => true]);
                }
                exit();
            } else {
                echo json_encode(['code' => 1020, 'message' => $valid, 'status' => false]);
            }
        }
    }

    /**Function __construct
     * @todo Exit user login
     **/
    public function logout()
    {
        sessionStart();
        $_SESSION['USERID'] = false;
        redirect('login');
    }

    /**Function __construct
     * @todo set middleware check login user
     **/
    public function checkLogin()
    {
        sessionStart();
        if (!$_SESSION['USERID']) {
            redirect('login');
        }
        return true;
    }

    public function block()
    {
        sessionStart();
        if (!$this->checkBlock()) {
            echo $this->view()->blade()->render('locked');
        } else {
            redirect('login');
        }
    }
}
