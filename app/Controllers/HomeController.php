<?php

namespace App\Controllers;

use Controllers;

/**Class Controller
    * @author  name: Masoud Ardi email: "masoudblackhat007@gmail.com"
    * @builder App\Controllers
    * @extends Controllers
**/
class HomeController extends Controllers
{
    /**Function index
        *@todo show dashboard when logined user
    **/
    public function index()
    {
        exit("ok");
        if (sessionCheck('USERID')) {
            redirect('login');
            exit();
        }
        redirect('admin');
        exit();
    }

    /**Function __construct
        * @todo when enter route unknown opened `route` `login` or `admin`
    **/
    public function notFound()
    {
        if (sessionCheck('USERID')) {
            redirect('admin');
        }
        redirect('login');
    }
}
