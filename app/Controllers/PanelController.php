<?php

namespace App\Controllers;

use Controllers;

/**Class Controller
    * @author  name: Masoud Ardi email: "masoudblackhat007@gmail.com"
    * @builder App\Controllers
    * @extends Controllers
**/
class PanelController extends Controllers
{
    private object $blade;

    public function __construct()
    {
        $this->blade = $this->view()->blade();
    }

    /**Function __construct
     * @todo show dashboard
    **/
    public function panel()
    {
        $blade = $this->view()->blade();
        $view = $this->blade->render('backend/admin/layouts/content');
        echo  $blade->render('backend/admin/panel', ['blade' => $blade,'view' => $view]);
    }
}
