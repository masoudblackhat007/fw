<?php

namespace App\Controllers;

use Controllers;
use systems\Helpers\QueryBuilder;

class CategoryController extends Controllers
{
    use QueryBuilder;

    private array $lang;
    private array $request;
    public function __construct()
    {
        $this->request = request();
        $lang = getConfig('default_language');
        $this->lang = lang($lang, ['category']);
    }

    public function create()
    {
        $_SESSION['VALIDATION_ERROR']  = $this->request(\Category::class);
        if (!empty($this->request) && empty($_SESSION['VALIDATION_ERROR'])) {
            $this->request['user_id'] = $_SESSION['USERID'];
            $this->loadModel(\QueryBuilderException::class)
                ->handel($this->request, $this->openQB()->from('categories'), 'category.success');
        }

        $view = $this->view()->blade()->render(
            'backend/admin/layouts/category/create',
            ['errors' => $_SESSION['VALIDATION_ERROR'],]
        );

        echo $this->view()->blade()->render('backend/admin/panel', ['blade' => $this->view()->blade()
            , 'view' => $view]);
    }

    public function update()
    {
    }
}
