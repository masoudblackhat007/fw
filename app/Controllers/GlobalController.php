<?php

namespace App\Controllers;

class GlobalController extends \Controllers
{
    public function loadMiddleware()
    {
        $this->middleware([\Login::class]);
    }
}
