<?php

use app\Services\User\Auth;

class Login
{
    private object $authServices;

    public function __construct()
    {
        $this->authServices = Auth::getInstance();
    }

    public function boot()
    {
        if (!$this->authServices->getUserId()) {
            redirect('login');
        }
    }
}
