<?php

namespace app\Exceptions;

class QueryBuilderException
{
    public function handel($request, $model, string $lang, string $action = 'insert')
    {
        sessionStart();
        try {
            $model->$action($request);
            $_SESSION['SUCCESS_MESSAGE'] = __($lang);
            unset($_SESSION['EXCEPTION_ERROR_MESSAGE']);
        } catch (Exception $exception) {
            unset($_SESSION['SUCCESS_MESSAGE']);
            $_SESSION['EXCEPTION_ERROR_MESSAGE'] = $exception->getMessage();
            error_log($exception->getMessage() . ".\r\n", 3, 'storage/logs/system.log');
            return $exception->getCode();
        }
    }
}
