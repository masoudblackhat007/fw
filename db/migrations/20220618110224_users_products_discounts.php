<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class UsersProductsDiscounts extends AbstractMigration
{
    public function up(): void
    {
        $table = $this->table('users_products_discounts', ['id' => false, 'primary_key'
        => ['product_id','user_id','discount_id']]);
        $table->addColumn('user_product_discount_id', 'integer')
            ->addColumn('product_id', 'integer')
            ->addForeignKey('product_id', 'products', 'product_id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION'])
            ->addColumn('user_id', 'integer')
            ->addForeignKey('user_id', 'users', 'user_id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION'])
            ->addColumn('discount_id', 'integer')
            ->addForeignKey(
                'discount_id',
                'discounts',
                'discount_id',
                ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']
            )
            ->create();
    }

    public function down()
    {
        $this->table('users_products_discounts')->drop()->save();
    }
}
