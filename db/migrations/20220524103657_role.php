<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class Role extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up(){
        $table = $this->table('roles', ['id' => false, 'primary_key' => 'role_id']);
        $table->addColumn('role_id','integer')
            ->addColumn('name','string')
            ->create();
    }

    public function down(){
        $this->table('roles_has_permissions')->drop()->save();
        $this->table('roles_has_users')->drop()->save();
        $this->table('modules_has_roles')->drop()->save();
        $this->table('roles')->drop()->save();
    }
}
