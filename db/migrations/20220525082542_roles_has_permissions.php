<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class RolesHasPermissions extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('roles_has_permissions', ['id' => false,'primary_key' => ['role_id','permission_id']]);
        $table->addColumn('role_id', 'integer', ['null' => false])
            ->addForeignKey('role_id', 'roles', 'role_id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION'])
            ->addColumn('permission_id', 'integer', ['null' => false])
            ->addForeignKey(
                'permission_id',
                'permissions',
                'permission_id',
                ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']
            )
            ->create();
    }

    public function down()
    {

        $this->table('roles_has_permissions')->drop()->save();
    }
}
