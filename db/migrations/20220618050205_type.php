<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class Type extends AbstractMigration
{
    public function up(): void
    {
        $table = $this->table('types', ['id' => false,'primary_key' => 'type_id']);
        $table->addColumn('type_id', 'integer')
            ->addColumn('name', 'string')
            ->addColumn('is_delete', 'boolean')
            ->create();
    }

    public function down()
    {
        $this->table('types')->drop()->save();
    }
}
