<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class InvoicesDetails extends AbstractMigration
{

    public function up(): void
    {
        $table = $this->table('invoices_details', ['id' => false
            ,'primary_key' => ['invoice_detail_id']]);
        $table->addColumn('invoice_detail_id', 'integer')
            ->addColumn('discount', 'string')
            ->addColumn('discount_type', 'string')
            ->addColumn('discount_symbol', 'string')
            ->addColumn('count', 'integer')
            ->addColumn('unit_price', 'decimal')
            ->addColumn('product_name', 'string')
            ->addColumn('total_price', 'string')
            ->addColumn('product_id', 'integer')
            ->addForeignKey('product_id', 'products', 'product_id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION'])
            ->addColumn('invoice_id', 'integer')
            ->addForeignKey('invoice_id', 'invoices', 'invoice_id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION'])
            ->create();
    }

    public function down()
    {
        $this->table('invoices_details')->drop()->save();
    }
}
