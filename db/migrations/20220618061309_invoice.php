<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class Invoice extends AbstractMigration
{
    public function up(): void
    {
        $table = $this->table('invoices', ['id' => false
            ,'primary_key' => ['invoice_id', 'user_id', 'status_id']]);
        $table->addColumn('invoice_id', 'integer')
            ->addColumn('user_id', 'integer')
            ->addForeignKey('user_id', 'users', 'user_id')
            ->addColumn('status_id', 'integer', ['null' => false])
            ->addForeignKey('status_id', 'status', 'status_id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION'])
            ->addColumn('created_at', 'datetime')
            ->addColumn('updated_at', 'datetime')
        ->create();
    }

    public function down()
    {
        $this->table('invoices')->drop()->save();
    }
}
