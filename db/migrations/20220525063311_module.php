<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class Module extends AbstractMigration
{
    public function up(){
        $table = $this->table('modules',['id'=>false,'primary_key' =>'module_id']);
        $table->addColumn('module_id','integer')
            ->addColumn('name','string')
            ->create();
    }

    public function down(){
        $this->table('modules_has_roles')->drop()->save();
        $this->table('modules')->drop()->save();
    }
}
