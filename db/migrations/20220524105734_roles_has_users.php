<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class RolesHasUsers extends AbstractMigration
{

    public function up(){
        $refTable = $this->table('roles_has_users',['id' => false, 'primary_key' => ['role_id','user_id']]);
        $refTable->addColumn('role_id', 'integer',['null'=>false])
            ->addForeignKey('role_id', 'roles', 'role_id', ['delete'=> 'NO_ACTION', 'update'=> 'NO_ACTION'])
            ->addColumn('user_id', 'integer',['null'=>false])
            ->addForeignKey('user_id', 'users', 'user_id', ['delete'=> 'NO_ACTION', 'update'=> 'NO_ACTION'])
            ->save();
    }

    public function down(){
        $this->table('roles_has_users')->drop()->save();
    }
}
