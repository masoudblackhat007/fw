<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class ModulesHasRoles extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('modules_has_roles', ['id' => false, 'primary_key' => ['role_id','module_id']]);
        $table->addColumn('role_id', 'integer', ['null' => false])
            ->addForeignKey('role_id', 'roles', 'role_id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION'])
            ->addColumn('module_id', 'integer', ['null' => false])
            ->addForeignKey('module_id', 'modules', 'module_id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION'])
            ->create();
    }

    public function down()
    {
        $this->table('modules_has_roles')->drop()->save();
    }
}
