<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class Files extends AbstractMigration
{
    public function up(): void
    {
        $table = $this->table('files', ['id' => false
            , 'primary_key' => ['file_id','user_id','category_id']]);
        $table->addColumn('file_id', 'integer')
            ->addColumn('url', 'string')
            ->addColumn('category_id', 'integer')
            ->addForeignKey(
                'category_id',
                'categories',
                'category_id',
                ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']
            )
            ->addColumn('user_id', 'integer')
            ->addForeignKey('user_id', 'users', 'user_id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION'])
            ->create();
    }

    public function down()
    {
        $this->table('files')->drop()->save();
    }
}
