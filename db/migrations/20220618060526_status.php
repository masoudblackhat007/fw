<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class Status extends AbstractMigration
{

    public function up(): void
    {
        $table = $this->table('status', ['id' => false,'primary_key' => 'status_id']);
        $table->addColumn('status_id', 'integer')
            ->addColumn('name', 'string')
            ->create();
    }

    public function down()
    {
        $this->table('status')->drop()->save();
    }
}
