<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class TicketsTypes extends AbstractMigration
{
    public function up(): void
    {
        $table = $this->table('tickets_types', ['id' => false, 'primary_key' => ['ticket_type_id']]);
        $table->addColumn('ticket_type_id', 'integer')
            ->addColumn('name', 'string')
            ->create();
    }

    public function down()
    {
        $this->table('tickets_types')->drop()->save();
    }
}
