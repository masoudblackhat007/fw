<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class DiscountsTypes extends AbstractMigration
{
    public function up(){
        $table = $this->table('discounts_types',['id'=>false,'primary_key' => ['discount_type_id','discount_id']]);
        $table->addColumn('discount_type_id','integer',['null'=>false])
            ->addColumn('name','string')
            ->addColumn('discount_id','integer',['null'=>false])
            ->addForeignKey('discount_id','discounts','discount_id',['delete'=> 'NO_ACTION', 'update'=> 'NO_ACTION'])
            ->create();
    }

    public function down(){
        $this->table('discounts_types')->drop()->save();
    }
}
