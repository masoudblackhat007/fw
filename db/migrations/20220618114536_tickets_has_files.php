<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class TicketsHasFiles extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up(): void
    {
        $table = $this->table('tickets_has_files', ['id' => false
            , 'primary_key' => ['ticket_id','file_id']]);
        $table->addColumn('ticket_id', 'integer')
            ->addForeignKey('ticket_id', 'tickets', 'ticket_id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION'])
            ->addColumn('file_id', 'integer')
            ->addForeignKey('file_id', 'files', 'file_id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION'])
            ->create();
    }

    public function down()
    {

    }
}
