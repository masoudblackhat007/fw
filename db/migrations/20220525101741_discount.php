<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class Discount extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('discounts', ['id' => false,'primary_key' => ['discount_id','user_id']]);
        $table->addColumn('discount_id', 'integer', ['null' => false])
            ->addColumn('user_id', 'integer', ['null' => false])
            ->addForeignKey('user_id', 'users', 'user_id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION'])
            ->addColumn('start_time', 'datetime')
            ->addColumn('end_time', 'datetime')
            ->addColumn('discount_code', 'string')
            ->addColumn('value', 'integer')
            ->addColumn('status', 'enum', ['values' => ['false','true']])
            ->addColumn('count', 'integer')
            ->create();
    }

    public function down()
    {
        $this->table('discounts_types')->drop()->save();
        $this->table('discounts')->drop()->save();
    }
}
