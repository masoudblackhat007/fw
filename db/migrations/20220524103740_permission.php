<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class Permission extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */

    public function up(){
        $table = $this->table('permissions',['id'=>false,'primary_key'=>'permission_id']);
        $table->addColumn('permission_id','integer')
            ->addColumn('name','string')
            ->create();

    }

    public function down(){
        $this->table('roles_has_permissions')->drop()->save();
        $this->table('permissions')->drop()->save();
     }
}
