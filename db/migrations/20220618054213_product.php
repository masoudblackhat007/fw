<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class Product extends AbstractMigration
{
    public function up(): void
    {
        $table = $this->table('products', ['id' => false, 'primary_key'
        => ['product_id','user_id','category_id','type_id','price_type_id']]);
        $table->addColumn('product_id', 'integer')
            ->addColumn('name', 'string')
            ->addColumn('description', 'string')
            ->addColumn('content', 'string')
            ->addColumn('user_id', 'integer', ['null' => false])
            ->addForeignKey('user_id', 'users', 'user_id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION'])
            ->addColumn('category_id', 'integer', ['null' => false])
            ->addForeignKey(
                'category_id',
                'categories',
                'category_id',
                ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']
            )
            ->addColumn('type_id', 'integer', ['null' => false])
            ->addForeignKey(
                'type_id',
                'types',
                'type_id',
                ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']
            )
            ->addColumn('is_visible', 'boolean')
            ->addColumn('price', 'decimal')
            ->addColumn('status', 'enum', ['values' => ['approval','desApproval']])
            ->addColumn('is_delete', 'boolean')
            ->addColumn('price_type_id', 'integer', ['null' => false])
            ->addForeignKey(
                'price_type_id',
                'prices_types',
                'price_type_id',
                ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']
            )
            ->create();
    }

    public function down()
    {
        $this->table('products')->drop()->save();
    }
}
