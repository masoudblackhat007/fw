<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class PricesTypes extends AbstractMigration
{
    public function up(): void
    {
        $table = $this->table('prices_types', ['id' => false, 'primary_key' => ['price_type_id']]);
        $table->addColumn('price_type_id', 'integer')
            ->addColumn('name', 'string')
            ->addColumn('symbol', 'string')
            ->addColumn('is_delete', 'boolean')
            ->create();
    }

    public function down()
    {
        $this->table('prices_types')->drop()->save();
    }
}
