<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class Ticket extends AbstractMigration
{
    public function up(): void
    {
        $table = $this->table('tickets', ['id' => false
            , 'primary_key' => ['ticket_id','user_id','department_id','ticket_type_id']]);
        $table->addColumn('ticket_id', 'integer')
            ->addColumn('user_id', 'integer')
            ->addForeignKey('user_id', 'users', 'user_id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION'])
            ->addColumn('department_id', 'integer')
            ->addForeignKey(
                'department_id',
                'departments',
                'department_id',
                ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']
            )
            ->addColumn('ticket_type_id', 'integer')
            ->addForeignKey(
                'ticket_type_id',
                'tickets_types',
                'ticket_type_id',
                ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']
            )
            ->addColumn('name', 'string')
            ->addColumn('content', 'text')
            ->create();
    }

    public function down()
    {
        $this->table('tickets')->drop()->save();
    }
}
