<!doctype html>
<html class="no-js " lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
    <title>:: آرئو مدیریت بوت استرپ 4 :: خانه</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon"> <!-- Favicon-->
    <link rel="stylesheet" href=" <?php echo route(getConfig('aero')) ?>/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo route(getConfig('aero')) ?>/plugins/jvectormap/jquery-jvectormap-2.0.3.min.css"/>
    <link rel="stylesheet" href="<?php echo route(getConfig('aero')) ?>/plugins/charts-c3/plugin.css"/>

    <link rel="stylesheet" href="<?php echo route(getConfig('aero')) ?>/plugins/morrisjs/morris.min.css" />
    <!-- Custom Css -->
    <link rel="stylesheet" href="<?php echo route(getConfig('aero')) ?>/css/style.min.css">
</head>

<body class="theme-blush">
<?php /**PATH C:\xampp\htdocs\fw\views\backend\admin\layouts/header.blade.php ENDPATH**/ ?>