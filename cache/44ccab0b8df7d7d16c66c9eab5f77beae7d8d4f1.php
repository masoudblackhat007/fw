﻿<?php view('header', 'admin/layouts') ?>
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="<?php echo route(getConfig('aero')) ?>/images/loader.svg" width="48" height="48" alt="Aero"></div>
        <p>لطفا صبر کنید...</p>
    </div>
</div>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<!-- Main Search -->
<div id="search">
    <button id="close" type="button" class="close btn btn-primary btn-icon btn-icon-mini btn-round">x</button>
    <form>
        <input type="search" value="" placeholder="جستجو..." />
        <button type="submit" class="btn btn-primary">جستجو</button>
    </form>
</div>

<?php view('sidebar', 'admin/layouts') ?>
<!-- Main Content -->

<section class="content">
    <?php view('content', 'admin/layouts') ?>
</section>
<?php view('footer', 'admin/layouts') ?><?php /**PATH C:\xampp\htdocs\FW1\views\admin/panel.blade.php ENDPATH**/ ?>