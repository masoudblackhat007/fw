<!-- Advanced Select -->

















































<form>
    <div class="form-group">
        <label for="title">Email address</label>
        <input type="text" class="form-control" name="title" id="title" placeholder="عنوان">

    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <div class="form-line">
                            <textarea rows="4" name="description" class="form-control no-resize"
                                      placeholder="لطفا توضیحات را تایپ کنید..."></textarea>
            <?php if(empty($_SESSION['VALIDATION_ERROR'])): ?>
                <div class="alert alert-warning"><?php echo $_SESSION['VALIDATION_ERROR']['name']['required']; ?></div>
            <?php endif; ?>
        </div>
    </div>
    <div class="form-group form-check">
        <input type="text" name="name" class="form-control" placeholder="نام دسته بندی">
        <label class="form-check-label" for="exampleCheck1">Check me out</label>
    </div>
    <div class="from-group">
        <?php if(!empty($_SESSION['EXCEPTION_ERROR_MESSAGE'])): ?>
            <div class="alert alert-warning"><?php echo e($_SESSION['EXCEPTION_ERROR_MESSAGE']); ?></div>
        <?php endif; ?>
        <?php
            unset($_SESSION['VALIDATION_ERROR'],
                    $_SESSION['EXCEPTION_ERROR_MESSAGE'],$_SESSION['SUCCESS_MESSAGE']);
        ?>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
<?php /**PATH C:\xampp\htdocs\fw\views/backend/admin/layouts/category/create.blade.php ENDPATH**/ ?>