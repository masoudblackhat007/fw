

<!-- Jquery Core Js -->
<script src="<?php echo route(getConfig('aero')) ?>/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) -->
<script src="<?php echo route(getConfig('aero')) ?>/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->

<script src="<?php echo route(getConfig('aero')) ?>/bundles/jvectormap.bundle.js"></script> <!-- JVectorMap Plugin Js -->
<script src="<?php echo route(getConfig('aero')) ?>/bundles/sparkline.bundle.js"></script> <!-- Sparkline Plugin Js -->
<script src="<?php echo route(getConfig('aero')) ?>/bundles/c3.bundle.js"></script>

<script src="<?php echo route(getConfig('aero')) ?>/bundles/mainscripts.bundle.js"></script>
<script src="<?php echo route(getConfig('aero')) ?>/js/pages/index.js"></script>


</body>

</html>
<?php /**PATH C:\xampp\htdocs\fw\views\backend\admin\layouts/footer.blade.php ENDPATH**/ ?>