﻿<!doctype html>
<html class="no-js " lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

    <title>:: Aero Bootstrap4 Admin :: Sign In</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- Custom Css -->
    <link rel="stylesheet"
          href="
          <?php

          use PhpRestfulApiResponse\Response;

          echo route(getConfig('aero')); ?>plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href=" <?php echo route(getConfig('aero')); ?>css/style.min.css">
</head>

<body class="theme-blush">

<div class="authentication">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-12">
                <?php
                if (isset($view)) {
                    echo $view->blade('views_layouts')->render('/validation_errors', ['errors' => $errors ?? []]);
                }
                ?>
                <form action="<?php echo route('/login'); ?>" class="card auth_form" method="post" id="login-form">
                    <div class="header">
                        <img class="logo" src=" <?php echo route(getConfig('aero')); ?>images/logo.svg" alt="">
                        <h5>ورود</h5>
                    </div>
                    <div class="body">
                        <div class="input-group mb-3">
                            <input type="hidden" id="csrf" name="csrf" value="<?php echo $csrf; ?>">
                        </div>
                        <div class="input-group mb-3">
                            <input type="text" name="name" class="form-control" placeholder="نام کاربری">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="zmdi zmdi-account-circle"></i></span>
                            </div>
                            <div id="name_required"></div>

                        </div>
                        <div class="input-group mb-3">

                            <input type="password" name="password" class="form-control" placeholder="رمزعبور">
                            <div class="input-group-append">

                                <span class="input-group-text"><i class="zmdi zmdi-lock"></i></span>
                            </div>
                        </div>
                        <div id="password_required"></div>
                        <div id="password_max"></div>
                        <div class="checkbox">
                            <input id="remember_me" name="remember" type="checkbox">
                            <label for="remember_me">مرا به خاطر بسپار</label>
                        </div>
                        <!-- recaptcha google  -->
                        <div class="g-recaptcha" data-sitekey="6LfGPjoeAAAAAF1YjtZ1mCdVAQI81b677qdVSyiW"></div>

                        <button name="submit" type="submit"
                                class="btn btn-primary btn-block waves-effect waves-light">ورود
                        </button>
                        <div class="signin_with mt-3">
                            <p class="mb-0">یا ثبت نام با استفاده از</p>
                            <button class="btn btn-primary btn-icon btn-icon-mini btn-round google"><i
                                        class="zmdi zmdi-google-plus"></i></button>
                            <a href="<?php echo route("register"); ?>" class="btn btn-primary">ثبت نام</a>
                        </div>
                    </div>
                </form>
                <div class="copyright text-center">
                    &copy;
                    <script>document.write(new Date().getFullYear())</script>
                    ,

                </div>
            </div>
            <div class="col-lg-8 col-sm-12">
                <div class="card">
                    <img src=" <?php echo route(getConfig('aero')); ?>images/signin.svg" alt="Sign In"/>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="<?php echo route(getConfig('aero')); ?>bundles/libscripts.bundle.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        <?php sessionStart(); ?>
        document.getElementById('csrf').value = "<?php echo $csrf; ?>"
        const frm = $('#login-form');
        frm.submit(function (e) {
            e.preventDefault();
            $.ajax({
                type: "post",
                url: frm.attr('action'),
                data: frm.serialize(),
                success: function (data) {

                    var obj = JSON.parse(data);
                    if(obj.code === 800){
                        window.location.href = '<?php echo getConfig(); ?>' + 'block';
                    }else if(obj.status){
                        window.location.href = '<?php echo getConfig(); ?>' + 'admin';
                    }else{
                        <?php $_SESSION['USERID'] = false; ?>
                        alert2("خطا", obj.message);
                    }
                },
                error: function (data) {
                    console.log('An error occurred.');
                    console.log(data);
                },
            });
        });

        function alert2(title, message) {
            swal({
                title: title,
                text: message,
                icon: "error",
                buttons: "Ok",
                dangerMode: true,
            });
        }
    });
</script>
</body>

</html>
<?php /**PATH C:\xampp\htdocs\fw\views/sign-in.blade.php ENDPATH**/ ?>