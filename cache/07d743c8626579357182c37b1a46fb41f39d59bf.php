

<!-- Jquery Core Js -->
<script src="<?php echo route(getConfig('aero')) ?>/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) -->
<script src="<?php echo route(getConfig('aero')) ?>/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->

<script src="<?php echo route(getConfig('aero')) ?>/bundles/jvectormap.bundle.js"></script> <!-- JVectorMap Plugin Js -->
<script src="<?php echo route(getConfig('aero')) ?>/bundles/sparkline.bundle.js"></script> <!-- Sparkline Plugin Js -->
<script src="<?php echo route(getConfig('aero')) ?>/bundles/c3.bundle.js"></script>

<script src="<?php echo route(getConfig('aero')) ?>/bundles/mainscripts.bundle.js"></script>
<script src="<?php echo route(getConfig('aero')) ?>/js/pages/index.js"></script>

<!-- Jquery Core Js -->

<script src="<?php echo route(getConfig('aero')) ?>/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> <!-- Bootstrap Colorpicker Js -->
<script src="<?php echo route(getConfig('aero')) ?>/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script> <!-- Input Mask Plugin Js -->
<script src="<?php echo route(getConfig('aero')) ?>/plugins/multi-select/js/jquery.multi-select.js"></script> <!-- Multi Select Plugin Js -->
<script src="<?php echo route(getConfig('aero')) ?>/plugins/jquery-spinner/js/jquery.spinner.js"></script> <!-- Jquery Spinner Plugin Js -->
<script src="<?php echo route(getConfig('aero')) ?>/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script> <!-- Bootstrap Tags Input Plugin Js -->
<script src="<?php echo route(getConfig('aero')) ?>/plugins/nouislider/nouislider.js"></script> <!-- noUISlider Plugin Js -->
<script src="<?php echo route(getConfig('aero')) ?>/plugins/select2/select2.min.js"></script> <!-- Select2 Js -->
<script src="<?php echo route(getConfig('aero')) ?>/js/pages/forms/advanced-form-elements.js"></script>

</body>

</html>
<?php /**PATH C:\xampp\htdocs\fw\views/backend/admin/layouts/footer.blade.php ENDPATH**/ ?>