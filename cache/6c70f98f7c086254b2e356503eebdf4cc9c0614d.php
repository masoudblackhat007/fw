﻿<!doctype html>
<html class="no-js " lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>:: Aero Bootstrap4 Admin ::</title>
<!-- Favicon-->
<link rel="icon" href="<?php echo route(getConfig('aero')) ?>favicon.ico" type="image/x-icon">
<!-- Custom Css -->
<link rel="stylesheet" href="<?php echo route(getConfig('aero')) ?>css/style.min.css">
<link rel="stylesheet" href="<?php echo route(getConfig('aero')) ?>plugins/bootstrap/css/bootstrap.min.css">
</head>

<body class="theme-blush">

<div class="authentication">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-12">
                <form class="card auth_form">
                    <div class="header">
                        <img class="l_profile rounded shadow" src="<?php echo route(getConfig('aero')) ?>images/profile_av.jpg" alt="">
                        <h5>آرش</h5>
                        <span>قفل شده</span>
                    </div>
                    <div class="body">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="جستجو...">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="zmdi zmdi-search"></i></span>
                            </div>
                        </div>
                        <a href="index.html" class="btn btn-primary btn-block waves-effect waves-light">برو به صفحه اصلی</a>                        
                        <div class="signin_with mt-3">
                            <a href="javascript:void(0);" class="link">آیا نیازمند کمک هستید؟</a>
                        </div>
                    </div>
                </form>
                <div class="copyright text-center">
                    &copy;
                    <script>document.write(new Date().getFullYear())</script>,
                    <span>راست چین شده توسط <a href="https://thememakker.com/" target="_blank">آرش خادملو</a></span>
                </div>
            </div>
            <div class="col-lg-8 col-sm-12">
                <div class="card">
                    <img src="<?php echo route(getConfig('aero')) ?>images/signin.svg" alt="Locked" />
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="<?php echo route(getConfig('aero')) ?>bundles/libscripts.bundle.js"></script>
<script src="<?php echo route(getConfig('aero')) ?>bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
</body>

</html><?php /**PATH C:\xampp\htdocs\framework\views/locked.blade.php ENDPATH**/ ?>