﻿<?php echo $blade->render('header'); ?>
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="<?php echo route(getConfig('aero')) ?>/images/loader.svg" width="48" height="48" alt="Aero"></div>
        <p>لطفا صبر کنید...</p>
    </div>
</div>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<!-- Main Search -->
<div id="search">
    <button id="close" type="button" class="close btn btn-primary btn-icon btn-icon-mini btn-round">x</button>
    <form>
      <input type="search" value="" placeholder="جستجو..." />
      <button type="submit" class="btn btn-primary">جستجو</button>
    </form>
</div>

<?php echo $blade->render('sidebar'); ?>

<section class="content blog-page">
<!--    --><?php //echo $blade->render('content'); ?>
</section>
<?php echo $blade->render('footer'); ?><?php /**PATH C:\xampp\htdocs\fw\views\backend\admin//panel.blade.php ENDPATH**/ ?>